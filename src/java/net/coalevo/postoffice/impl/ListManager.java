/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.postoffice.model.NoSuchListException;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * This class implements a manager for lists.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ListManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(ListManager.class.getName());
  private PostOfficeStore m_Store;

  private LRUCacheMap<String, MailingListImpl> m_ListsCache;
  private LRUCacheMap<AgentIdentifier, List<String>> m_MembershipsCache;

  public ListManager(PostOfficeStore store, int listscachesize, int memcachesize) {
    m_Store = store;
    m_ListsCache = new LRUCacheMap<String, MailingListImpl>(listscachesize);
    m_MembershipsCache = new LRUCacheMap<AgentIdentifier, List<String>>(memcachesize);
  }//constructor

  public void setListsCacheCeiling(int lcs) {
    m_ListsCache.setCeiling(lcs);
  }//setListsCacheCeiling

  public void setMembershipsCacheCeiling(int mcs) {
    m_MembershipsCache.setCeiling(mcs);
  }//setMembershipsCacheCeiling

  public void maintain() {
    m_ListsCache.clear();
    m_MembershipsCache.clear();
  }//maintain

  public boolean addList(String identifier, boolean system, boolean allowmem)
      throws PostOfficeServiceException {

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("list_id", identifier);
    params.put("issys", ((system) ? "1" : "0"));
    params.put("allowmem", (allowmem) ? "1" : "0");

    //Remove from db
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("addList", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "addList()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("ListManager.addfailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    return true;
  }//addList

  public MailingListImpl getList(String identifier)
      throws NoSuchListException, PostOfficeServiceException {
    MailingListImpl ml = m_ListsCache.get(identifier);
    if (ml == null) {
      ml = new MailingListImpl(m_Store, identifier);
    }
    return ml;
  }//getList

  public boolean existsList(String identifier) throws PostOfficeServiceException {
    if (m_ListsCache.containsKey(identifier)) {
      return true;
    }
    //Build list
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("list_id", identifier);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("existsList", params);
      return (rs.next() && rs.getInt(1) > 0);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "existsList()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("ListManager.existsfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//existsList

  public boolean removeList(String identifier)
      throws PostOfficeServiceException {

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("list_id", identifier);

    //Remove from db
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("removeList", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "removeList()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("ListManager.removefailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    //Remove from cache (should not trigger expel handler)
    m_ListsCache.remove(identifier);
    return true;
  }//removeList

  public Set<String> getLists(boolean system) throws PostOfficeServiceException {
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("issys", ((system) ? "1" : "0"));
    TreeSet<String> l = new TreeSet<String>();

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getLists", params);
      while (rs.next()) {
        l.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getLists()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("ListManager.getlistsfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return l;
  }//getLists

  public List<String> getListMemberships(AgentIdentifier aid)
      throws PostOfficeServiceException {
    ResultSet rs = null;
    List<String> mems = m_MembershipsCache.get(aid);
    if (mems == null) {
      mems = new ArrayList<String>();
      //Query from DB
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("agent_id", aid.getIdentifier());

      //Remove from db
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getAllMyLists", params);
        while (rs.next()) {
          mems.add(rs.getString(1));
        }
        //cache before returning
        m_MembershipsCache.put(aid, mems);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "getListMemberships()", ex);
        throw new PostOfficeServiceException(Activator.getBundleMessages().get("ListManager.membershipsfailed"), ex);
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
      }
    }
    return mems;
  }//getMemberships

}//class ListManager
