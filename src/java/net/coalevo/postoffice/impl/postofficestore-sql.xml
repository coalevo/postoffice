<?xml version="1.0"?>

<l:library xmlns:l="http://www.slamb.org/axamol/sql-library/library"
           xmlns:s="http://www.slamb.org/axamol/sql-library/statement" ps-cache="35">
  <l:name>PostOffice Store</l:name>

  <s:query name="validate">
    <l:description>
      Validation query for database connections.
    </l:description>
    <s:sql databases="mysql">
      select 1
    </s:sql>
    <s:sql databases="derby">
      values(1)
    </s:sql>
  </s:query>

  <!-- Check schema exists -->
  <s:query name="existsSchema">
    <l:description>
      Checks if the schema exists.
    </l:description>

    <s:sql databases="derby mysql">
      select count(*) from cpostoffice.poboxes
    </s:sql>

  </s:query>

  <!-- Store/Schema related statements -->
  <s:create name="createPostOfficeSchema" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates the schema for all post office related tables.
    </l:description>

    <s:sql databases="derby">
      create schema cpostoffice
    </s:sql>
    <s:sql databases="mysql">
      create schema cpostoffice default character set utf8 collate utf8_bin
    </s:sql>
  </s:create>

  <!-- Create Tables -->
  <s:create name="createPOBoxTable" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates the table for storing post boxes.
    </l:description>
    <s:sql databases="derby">
      create table cpostoffice.poboxes (
      pobox_id varchar(255) not null,
      language varchar(3),
      forwarding smallint,
      isprivate smallint,
      notify smallint,
      idxreb smallint,
      primary key (pobox_id)
      )
    </s:sql>
    <s:sql databases="mysql">
      create table cpostoffice.poboxes (
      pobox_id varchar(255) not null,
      language varchar(3),
      forwarding smallint,
      isprivate smallint,
      notify smallint,
      idxreb smallint,
      primary key (pobox_id)
      ) engine = innodb;
    </s:sql>
  </s:create>

  <s:create name="createBlacklistTable" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates the table for storing blacklists.
    </l:description>
    <s:sql databases="derby">
      create table cpostoffice.blacklists (
      pobox_id varchar(255) not null,
      agent_id varchar(255) not null,
      foreign key (pobox_id) references cpostoffice.poboxes on delete cascade
      )
    </s:sql>
    <s:sql databases="mysql">
      create table cpostoffice.blacklists (
      pobox_id varchar(255) not null,
      agent_id varchar(255) not null,
      foreign key (pobox_id) references cpostoffice.poboxes(pobox_id) on delete cascade
      ) engine = innodb;
    </s:sql>
  </s:create>

  <s:create name="createMessageDescriptorTable" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates the table for storing message descriptors.
    </l:description>
    <s:sql databases="derby">
      create table cpostoffice.message_descriptors (
      pobox_id varchar(255) not null,
      msg_id varchar(255) not null,
      ancestor_id varchar(255),
      mtype varchar(255) not null,
      deposited bigint,
      isread smallint,
      from_id varchar(255) not null,
      to_id varchar(255),
      via varchar(255),
      subject varchar(255),
      format varchar(255),
      primary key (pobox_id,msg_id),
      foreign key (pobox_id) references cpostoffice.poboxes on delete cascade
      )
    </s:sql>
    <s:sql databases="mysql">
      create table cpostoffice.message_descriptors (
      pobox_id varchar(255) not null,
      msg_id varchar(255) not null,
      ancestor_id varchar(255),
      mtype varchar(255) not null,
      deposited bigint,
      isread smallint,
      from_id varchar(255) not null,
      to_id varchar(255),
      via varchar(255),
      subject varchar(255),
      format varchar(255),
      primary key (pobox_id,msg_id),
      foreign key (pobox_id) references cpostoffice.poboxes(pobox_id) on delete cascade
      ) engine = innodb;
    </s:sql>
  </s:create>

  <s:create name="createMessageBodyTable" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates the table for storing message bodies.
    </l:description>

    <s:sql databases="derby">
      create table cpostoffice.message_bodies (
      pobox_id varchar(255) not null,
      msg_id varchar(255) not null,
      document long varchar,
      primary key (pobox_id,msg_id),
      foreign key (pobox_id,msg_id) references cpostoffice.message_descriptors on delete cascade
      )
    </s:sql>
    <s:sql databases="mysql">
      create table cpostoffice.message_bodies (
      pobox_id varchar(255) not null,
      msg_id varchar(255) not null,
      document long varchar,
      primary key (pobox_id,msg_id),
      foreign key (pobox_id,msg_id) references cpostoffice.message_descriptors(pobox_id,msg_id) on delete cascade
      ) engine = innodb;
    </s:sql>
  </s:create>

  <s:create name="createMailingListTable" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates the table for storing mailing lists.
    </l:description>

    <s:sql databases="derby">
      create table cpostoffice.mailinglists (
      list_id varchar(255) not null,
      issys smallint,
      allowmem smallint,
      primary key(list_id)
      )
    </s:sql>
    <s:sql databases="mysql">
      create table cpostoffice.mailinglists (
      list_id varchar(255) not null,
      issys smallint,
      allowmem smallint,
      primary key(list_id)
      ) engine = innodb;
    </s:sql>
  </s:create>

  <s:create name="createMailingListRecipientsTable" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates the table for storing mailing list recipients.
    </l:description>

    <s:sql databases="derby">
      create table cpostoffice.mailinglist_recipients (
      list_id varchar(255) not null,
      agent_id varchar(255) not null,
      foreign key (list_id) references cpostoffice.mailinglists on delete cascade
      )
    </s:sql>
    <s:sql databases="mysql">
      create table cpostoffice.mailinglist_recipients (
      list_id varchar(255) not null,
      agent_id varchar(255) not null,
      foreign key (list_id) references cpostoffice.mailinglists(list_id) on delete cascade
      ) engine = innodb;
    </s:sql>
  </s:create>

  <!-- END: Create Tables -->

  <!-- START: Create indices -->

  <s:create name="createDepositedIndex" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates an index for faster deposit timestamp based operations.
    </l:description>

    <s:sql databases="derby">
      create index cpostoffice.message_descriptors_idx on cpostoffice.message_descriptors(deposited)
    </s:sql>
    <s:sql databases="mysql">
      create index message_descriptors_idx on cpostoffice.message_descriptors(deposited)
    </s:sql>
  </s:create>

  <!-- END: Create Indices-->

  <!-- POBox handling -->

  <s:update name="addPOBox" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Adds a new POBox.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="language" type="varchar"/>
    <s:param name="forwarding" type="smallint"/>
    <s:param name="private" type="smallint"/>
    <s:param name="notify" type="smallint"/>
    <s:param name="idxreb" type="smallint"/>

    <s:sql databases="derby mysql">
      insert into cpostoffice.poboxes values (
      <s:bind param="pobox_id"/>,
      <s:bind param="language"/>,
      <s:bind param="forwarding"/>,
      <s:bind param="private"/>,
      <s:bind param="notify"/>,
      <s:bind param="idxreb"/>
      )
    </s:sql>
  </s:update>

  <s:update name="removePOBox" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Removes an existing POBox.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      delete from cpostoffice.poboxes
      where pobox_id=
      <s:bind param="pobox_id"/>
    </s:sql>
  </s:update>

  <s:query name="getPOBox" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Retrieves an existing POBox by owner.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select * from cpostoffice.poboxes
      where pobox_id=
      <s:bind param="pobox_id"/>
    </s:sql>
  </s:query>

  <s:query name="existsPOBox" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Checks if there is an existing POBox for the given id.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select count(*) from cpostoffice.poboxes
      where pobox_id=
      <s:bind param="pobox_id"/>
    </s:sql>
  </s:query>

  <s:query name="listPOBoxes" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Lists POBoxes by id.
    </l:description>

    <s:sql databases="derby mysql">
      select pobox_id from cpostoffice.poboxes
    </s:sql>
  </s:query>

  <s:update name="updatePOBox" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Updates an existing POBox
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="language" type="varchar"/>
    <s:param name="forwarding" type="smallint"/>
    <s:param name="private" type="smallint"/>
    <s:param name="notify" type="smallint"/>
    <s:param name="idxreb" type="smallint"/>

    <s:sql databases="derby mysql">
      update cpostoffice.poboxes set
      language=<s:bind param="language"/>,
      forwarding=<s:bind param="forwarding"/>,
      isprivate=<s:bind param="private"/>,
      notify=<s:bind param="notify"/>,
      idxreb=
      <s:bind param="idxreb"/>
      where pobox_id=
      <s:bind param="pobox_id"/>
    </s:sql>
  </s:update>

  <s:query name="listAllNotifying" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Lists all the post boxes that requested notification.
    </l:description>

    <s:sql databases="derby mysql">
      select distinct pobox_id from cpostoffice.poboxes
      where notify=1
    </s:sql>
  </s:query>

  <!-- END: POBox handling -->

  <!-- Blacklist handling -->

  <s:update name="addBlacklisted" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Adds a blacklist entry.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="agent_id" type="varchar"/>

    <s:sql databases="derby mysql">
      insert into cpostoffice.blacklists values (
      <s:bind param="pobox_id"/>,
      <s:bind param="agent_id"/>
      )
    </s:sql>
  </s:update>

  <s:update name="removeBlacklisted" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Removes a blacklist entry.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="agent_id" type="varchar"/>

    <s:sql databases="derby mysql">
      delete from cpostoffice.blacklists
      where pobox_id=
      <s:bind param="pobox_id"/>
      and agent_id=
      <s:bind param="agent_id"/>
    </s:sql>
  </s:update>

  <s:query name="getBlacklisted" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Retrieves an blacklisted agents for a postbox.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select agent_id from cpostoffice.blacklists
      where pobox_id=
      <s:bind param="pobox_id"/>
    </s:sql>
  </s:query>

  <!-- END: Blacklist handling -->

  <!-- Message Descriptor Handling -->

  <s:update name="addMessage" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Creates a descriptor for a new message.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="msg_id" type="varchar"/>
    <s:param name="ancestor" type="varchar"/>
    <s:param name="mtype" type="varchar"/>
    <s:param name="deposited" type="bigint"/>
    <s:param name="read" type="smallint"/>
    <s:param name="from" type="varchar"/>
    <s:param name="via" type="varchar"/>
    <s:param name="to" type="varchar"/>
    <s:param name="subject" type="varchar"/>
    <s:param name="format" type="varchar"/>

    <s:sql databases="derby mysql">
      insert into cpostoffice.message_descriptors values(
      <s:bind param="pobox_id"/>,
      <s:bind param="msg_id"/>,
      <s:bind param="ancestor"/>,
      <s:bind param="mtype"/>,
      <s:bind param="deposited"/>,
      <s:bind param="read"/>,
      <s:bind param="from"/>,
      <s:bind param="to"/>,
      <s:bind param="via"/>,
      <s:bind param="subject"/>,
      <s:bind param="format"/>
      )
    </s:sql>
  </s:update>

  <s:query name="getMessage" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Returns a message descriptor.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="msg_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select * from cpostoffice.message_descriptors
      where pobox_id=
      <s:bind param="pobox_id"/>
      and msg_id=
      <s:bind param="msg_id"/>
    </s:sql>
  </s:query>

  <s:update name="removeMessage" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Deletes a message.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="msg_id" type="varchar"/>

    <s:sql databases="derby mysql">
      delete from cpostoffice.message_descriptors
      where pobox_id=
      <s:bind param="pobox_id"/>
      and msg_id=
      <s:bind param="msg_id"/>
    </s:sql>
  </s:update>

  <s:update name="markRead" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Marks a message as read, updating the descriptor.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="msg_id" type="varchar"/>

    <s:sql databases="derby mysql">
      update cpostoffice.message_descriptors
      set isread=1
      where pobox_id=
      <s:bind param="pobox_id"/>
      and msg_id=
      <s:bind param="msg_id"/>
    </s:sql>
  </s:update>

  <s:query name="listUnread" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Lists unread messages in a postbox.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select msg_id from cpostoffice.message_descriptors
      where pobox_id=
      <s:bind param="pobox_id"/>
      and isread=0 order by deposited
    </s:sql>
  </s:query>

  <s:query name="hasUnread" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Checks if there are unread messages in a postbox.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select count(msg_id) from cpostoffice.message_descriptors
      where pobox_id=
      <s:bind param="pobox_id"/>
      and isread=0
    </s:sql>
  </s:query>

  <s:query name="listMessages" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Lists all messages in a postbox.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select msg_id from cpostoffice.message_descriptors
      where pobox_id=
      <s:bind param="pobox_id"/>
      order by deposited desc
    </s:sql>
  </s:query>

  <s:query name="getMessageCount">
    <l:description>
      Returns the message count.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select count(*) from cpostoffice.message_descriptors
      where pobox_id=
      <s:bind param="pobox_id"/>
    </s:sql>
  </s:query>

  <!-- END: Message Descriptor Handling -->

  <!-- Message Bodies Handling -->

  <s:update name="addMessageBody" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Adds a message body.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="msg_id" type="varchar"/>
    <s:param name="document" type="longvarchar"/>

    <s:sql databases="derby mysql">
      insert into cpostoffice.message_bodies values(
      <s:bind param="pobox_id"/>,
      <s:bind param="msg_id"/>,
      <s:bind param="document"/>
      )
    </s:sql>
  </s:update>

  <s:query name="getMessageBody" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Retrieves a message body.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>
    <s:param name="msg_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select document from cpostoffice.message_bodies
      where pobox_id=
      <s:bind param="pobox_id"/>
      and msg_id=
      <s:bind param="msg_id"/>
    </s:sql>
  </s:query>

  <!-- END: Message Bodies Handling -->

  <!-- START: Index handling -->
  <s:query name="listMessagesForIndex" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Lists all messages in a postbox for indexing.
    </l:description>
    <s:param name="pobox_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select cpostoffice.message_descriptors.*,cpostoffice.message_bodies.document
      from cpostoffice.message_descriptors,cpostoffice.message_bodies
      where cpostoffice.message_descriptors.pobox_id=
      <s:bind param="pobox_id"/>
      and cpostoffice.message_descriptors.pobox_id=cpostoffice.message_bodies.pobox_id
      and cpostoffice.message_descriptors.msg_id=cpostoffice.message_bodies.msg_id
    </s:sql>
  </s:query>
  <!-- END: Index handling -->

  <!-- Mailing list handling -->

  <s:update name="addList" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Adds a mailing list.
    </l:description>
    <s:param name="list_id" type="varchar"/>
    <s:param name="issys" type="smallint"/>
    <s:param name="allowmem" type="smallint"/>

    <s:sql databases="derby mysql">
      insert into cpostoffice.mailinglists values (
      <s:bind param="list_id"/>,
      <s:bind param="issys"/>,
      <s:bind param="allowmem"/>,
      )
    </s:sql>
  </s:update>

  <s:update name="removeList" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Removes a mailing list.
    </l:description>
    <s:param name="list_id" type="varchar"/>

    <s:sql databases="derby mysql">
      delete from cpostoffice.mailinglists
      where list_id=
      <s:bind param="list_id"/>
    </s:sql>
  </s:update>

  <s:query name="getLists" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Retrieves all mailing lists.
    </l:description>
    <s:param name="issys" type="smallint"/>

    <s:sql databases="derby mysql">
      select list_id from cpostoffice.mailinglists
      where issys=
      <s:bind param="issys"/>
    </s:sql>
  </s:query>

  <s:query name="existsList" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Checks if a given list exists.
    </l:description>
    <s:param name="list_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select count(*) from cpostoffice.mailinglists
      where list_id=
      <s:bind param="list_id"/>
    </s:sql>
  </s:query>

  <s:query name="isSystemList" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Checks if a given list is flagged as system list.
    </l:description>
    <s:param name="list_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select issys from cpostoffice.mailinglists
      where list_id=
      <s:bind param="list_id"/>
    </s:sql>
  </s:query>

  <s:query name="getListProperties" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Retrieves properties of a list.
    </l:description>
    <s:param name="list_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select issys,allowmem from cpostoffice.mailinglists
      where list_id=
      <s:bind param="list_id"/>
    </s:sql>
  </s:query>

  <!-- END: Mailinglist handling -->

  <!-- Mailinglist recipients handling -->

  <s:update name="addRecipient" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Adds a mailing list recipient.
    </l:description>
    <s:param name="list_id" type="varchar"/>
    <s:param name="agent_id" type="varchar"/>

    <s:sql databases="derby mysql">
      insert into cpostoffice.mailinglist_recipients values (
      <s:bind param="list_id"/>,
      <s:bind param="agent_id"/>
      )
    </s:sql>
  </s:update>

  <s:update name="removeRecipient" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Removes a mailing list recipient.
    </l:description>
    <s:param name="list_id" type="varchar"/>
    <s:param name="agent_id" type="varchar"/>

    <s:sql databases="derby mysql">
      delete from cpostoffice.mailinglist_recipients
      where list_id=
      <s:bind param="list_id"/>
      and agent_id=
      <s:bind param="agent_id"/>
    </s:sql>
  </s:update>

  <s:query name="getRecipients" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Retrieves the recipients of a mailing list.
    </l:description>
    <s:param name="list_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select agent_id from cpostoffice.mailinglist_recipients
      where list_id=
      <s:bind param="list_id"/>
    </s:sql>
  </s:query>

  <!-- END: Mailinglist recipients handling  -->

  <!-- START: Membership handling -->

  <s:query name="getAllMyLists" xmlns:s="http://www.slamb.org/axamol/sql-library/statement">
    <l:description>
      Retrieves all mailing lists a user is a member of.
    </l:description>
    <s:param name="agent_id" type="varchar"/>

    <s:sql databases="derby mysql">
      select list_id from cpostoffice.mailinglist_recipients
      where agent_id=
      <s:bind param="agent_id"/>
      order by list_id
    </s:sql>
  </s:query>

  <!-- END: Membership handling -->

</l:library>