/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.postoffice.model.MailingList;
import net.coalevo.postoffice.model.NoSuchListException;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * This class implements a {@link MailingList}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MailingListImpl
    implements MailingList {

  private Marker m_LogMarker = MarkerFactory.getMarker(MailingListImpl.class.getName());
  private PostOfficeStore m_Store;
  private String m_Identifier;
  private Set<AgentIdentifier> m_AgentIdentifiers;
  private boolean m_System;
  private Map<String, String> m_Params;
  private boolean m_AllowsMembers;

  public MailingListImpl(PostOfficeStore store, String identifier) {
    m_Store = store;
    m_Identifier = identifier;
    prepareList();
  }//constructor

  public MailingListImpl(PostOfficeStore store, String identifier, boolean system) {
    this(store, identifier);
    m_System = system;
  }//constructor

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public boolean add(AgentIdentifier aid) {
    if (m_AgentIdentifiers.add(aid)) {
      //do persistent add
      LibraryConnection lc = null;
      synchronized (m_Params) {
        m_Params.put("agent_id", aid.getIdentifier());
        try {
          lc = m_Store.leaseConnection();
          lc.executeUpdate(ADD, m_Params);
          return true;
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "add()", ex);
        } finally {
          m_Store.releaseConnection(lc);
          m_Params.remove("agent_id");
        }
      }
    }
    return false;
  }//add

  public void add(Set<AgentIdentifier> identifiers) {
    for (AgentIdentifier aid : identifiers) {
      add(aid);
    }
  }//add

  public boolean remove(AgentIdentifier aid) {
    if (m_AgentIdentifiers.remove(aid)) {
      LibraryConnection lc = null;
      synchronized (m_Params) {
        m_Params.put("agent_id", aid.getIdentifier());
        try {
          lc = m_Store.leaseConnection();
          lc.executeUpdate(REMOVE, m_Params);
          return true;
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "remove()", ex);
        } finally {
          m_Store.releaseConnection(lc);
          m_Params.remove("agent_id");
        }
      }
    }
    return false;
  }//remove

  public void remove(Set<AgentIdentifier> identifiers) {
    for (AgentIdentifier aid : identifiers) {
      remove(aid);
    }
  }//remove

  public Set<AgentIdentifier> getRecipients() {
    return Collections.unmodifiableSet(m_AgentIdentifiers);
  }//getRecipients

  public boolean isSystem() {
    return m_System;
  }//isSystem

  public boolean isAllowsMembers() {
    return m_AllowsMembers;
  }//allowsMembers

  public boolean isMember(AgentIdentifier aid) {
    return m_AgentIdentifiers.contains(aid);
  }//isMember

  private void prepareList() {
    m_AgentIdentifiers = new HashSet<AgentIdentifier>();
    m_Params = new HashMap<String, String>();
    m_Params.put("list_id", m_Identifier);

    refresh();
  }//prepareList

  private void refresh() {
    m_AgentIdentifiers.clear();
    LibraryConnection lc = null;
    ResultSet rs = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery(LIST, m_Params);
      while (rs.next()) {
        m_AgentIdentifiers.add(m_Store.getAgentIdentifier(rs.getString(1)));
      }
      SqlUtils.close(rs);
      //get system flag
      rs = lc.executeQuery("getListProperties", m_Params);
      if (rs.next()) {
        m_System = (rs.getInt(1) == 1);
        m_AllowsMembers = (rs.getInt(2) == 1);
      } else {
        throw new NoSuchListException(m_Params.get("list_id"));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "refresh()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//refresh

  private static String ADD = "addRecipient";
  private static String REMOVE = "removeRecipient";
  private static String LIST = "getRecipients";

}//class MailingListImpl
