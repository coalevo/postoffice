/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.postoffice.model.*;
import net.coalevo.postoffice.service.PostOfficeService;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.system.service.MailService;
import net.coalevo.system.service.MailServiceException;
import net.coalevo.text.model.TransformationException;
import net.coalevo.text.service.TransformationService;
import net.coalevo.userdata.service.UserdataService;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.*;

/**
 * This class implements {@link PostOfficeService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PostOfficeServiceImpl
    extends BaseService
    implements PostOfficeService, Maintainable {

  private Marker m_LogMarker;
  private PostOfficeStore m_Store;

  private POBoxManager m_POBoxManager;
  private ListManager m_ListManager;
  private MessageManager m_MessageManager;
  private NotificationManager m_NotificationManager;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private Map<String, EditableMessage> m_MessageTracker;

  public PostOfficeServiceImpl(PostOfficeStore store) {
    super(PostOfficeService.class.getName(), ACTIONS);
    m_LogMarker = MarkerFactory.getMarker(PostOfficeServiceImpl.class.getName());
    m_Store = store;
  }//constructor

  public boolean activate(BundleContext bc) {
    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/PostOfficeService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    //3. Notification
    m_NotificationManager = new NotificationManager(m_ServiceAgentProxy, m_Store.getPOBoxManager());
    m_NotificationManager.activate(bc);
    Activator.getServices().setNotificationManager(m_NotificationManager);

    //prepare refs
    m_POBoxManager = m_Store.getPOBoxManager();
    m_MessageManager = m_Store.getMessageManager();
    m_ListManager = m_Store.getListManager();

    //prepare tracker
    m_MessageTracker = new WeakHashMap<String, EditableMessage>();

    return true;
  }//activate

  public boolean deactivate() {

    if (m_NotificationManager != null) {
      m_NotificationManager.deactivate();
      m_NotificationManager = null;
    }
    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }

    //refs
    m_POBoxManager = null;
    m_MessageManager = null;
    m_ListManager = null;

    //Tracker
    m_MessageTracker = null;

    return true;
  }//deactivate

  public POBox createPostBox(Agent caller,
                             AgentIdentifier aid,
                             Locale l,
                             boolean forw,
                             boolean priv,
                             boolean notify)
      throws SecurityException, PostOfficeServiceException {

    //1. Check auth
    if (caller.getAgentIdentifier().equals(aid)) {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    } else {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_POSTBOX);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("error.action", "action", CREATE_POSTBOX.getIdentifier(), "caller", caller.getIdentifier()), ex);
        return null;
      }
    }

    return m_POBoxManager.createPOBox(aid, l, forw, priv, notify);
  }//createPostBox

  public POBox getPostBox(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchPOBoxException, PostOfficeServiceException {
    //1. Check auth
    if (caller.getAgentIdentifier().equals(aid)) {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    } else {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_POSTBOX);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("error.action", "action", GET_POSTBOX.getIdentifier(), "caller", caller.getIdentifier()), ex);
        return null;
      }
    }

    return m_POBoxManager.getPOBox(aid);
  }//getPostBox

  public boolean removePostBox(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchPOBoxException, PostOfficeServiceException {

    //1. Check auth
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_POSTBOX);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("error.action", "action", REMOVE_POSTBOX.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return false;
    }
    return m_POBoxManager.removePOBox(aid);
  }//removePostBox

  public MailingList createList(Agent caller, String identifier, boolean allowmem)
      throws SecurityException, NoSuchListException, PostOfficeServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_LIST);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("error.action", "action", CREATE_LIST.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return null;
    }
    m_ListManager.addList(identifier, false, allowmem);
    return m_ListManager.getList(identifier);
  }//createList

  public void removeList(Agent caller, String identifier)
      throws SecurityException, NoSuchListException, PostOfficeServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_LIST);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("error.action", "action", REMOVE_LIST.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return;
    }
    m_ListManager.removeList(identifier);
  }//removeList

  public MailingList getList(Agent caller, String identifier)
      throws SecurityException, NoSuchListException, PostOfficeServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_LIST);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("error.action", "action", GET_LIST.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return null;
    }
    return m_ListManager.getList(identifier);
  }//getList

  public Set<String> getLists(Agent caller, boolean system)
      throws SecurityException, PostOfficeServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(
          m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller),
          GET_LISTS
      );
    } catch (NoSuchActionException ex) {
      Activator.log().error(
          m_LogMarker,
          Activator.getBundleMessages().get(
              "error.action",
              "action",
              GET_LISTS.getIdentifier(),
              "caller",
              caller.getIdentifier()
          ),
          ex
      );
      return null;
    }
    return m_ListManager.getLists(system);
  }//getLists

  public List<String> getListMemberships(Agent caller, AgentIdentifier aid)
      throws SecurityException, PostOfficeServiceException {

    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(
            m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller),
            GET_LIST_MEMBERSHIPS
        );
      } catch (NoSuchActionException ex) {
        Activator.log().error(
            m_LogMarker,
            Activator.getBundleMessages().get(
                "error.action", "action",
                GET_LIST_MEMBERSHIPS.getIdentifier(), "caller",
                caller.getIdentifier()
            )
            , ex);
        return null;
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    return m_ListManager.getListMemberships(aid);
  }//getListMemberships

  public boolean isSendToListAllowed(Agent caller, String listname)
      throws SecurityException, PostOfficeServiceException {

    //1. Check authenticity
    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);

    //2. Get list and return authorization
    MailingListImpl mlist = m_ListManager.getList(listname);
    return mlist.isAllowsMembers() && mlist.isMember(caller.getAgentIdentifier());
  }//isList

  public EditableMessage beginMessage(Agent caller, AgentIdentifier to)
      throws SecurityException, PostOfficeServiceException {

    //1. Check auth
    try {
      m_PolicyProxy.ensureAuthorization(
          m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller),
          BEGIN_MESSAGE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(
          m_LogMarker,
          Activator.getBundleMessages().get(
              "error.action",
              "action",
              BEGIN_MESSAGE.getIdentifier(),
              "caller",
              caller.getIdentifier()
          ),
          ex
      );
      return null;
    }
    if (caller.getAgentIdentifier().equals(to)) {
      throw new IllegalArgumentException();
    }

    //1. id and check existence
    String id = Activator.getServices().getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID();

    if (!m_POBoxManager.existsPOBox(to)) {
      throw new NoSuchPOBoxException(to.getIdentifier());
    }
    //2. check privacy
    if(! (caller instanceof ServiceAgent)) {
      POBox pobox = m_POBoxManager.getPOBox(to);
      if (pobox.isBlacklisted(caller.getAgentIdentifier())) {
        throw new PrivacyException();
      }
      if (pobox.isPrivate()) {
        PresenceService ps = Activator.getServices().getPresenceService(ServiceMediator.NO_WAIT);
        if (!ps.checkSubscription(m_ServiceAgentProxy.getAuthenticPeer(), caller, to)) {
          throw new PrivacyException();
        }
      }
    }
    EditableMessage m = new EditableMessageImpl(
        id,
        caller.getAgentIdentifier(),
        to,
        MessageTypes.MESSAGE
    );

    //track message
    m_MessageTracker.put(id, m);

    return m;
  }//beginMessage

  public EditableMessage beginListMessage(Agent caller, String listid)
      throws SecurityException, NoSuchListException, PostOfficeServiceException {

    //1. check if list exists
    if (!m_ListManager.existsList(listid)) {
      throw new NoSuchListException(listid);
    }

    //2. Get list and check authorization
    MailingListImpl mlist = m_ListManager.getList(listid);
    // member check
    if (!(mlist.isAllowsMembers() && mlist.isMember(caller.getAgentIdentifier()))) {
      // policy check
      try {
        m_PolicyProxy.ensureAuthorization(
            m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller),
            BEGIN_LISTMESSAGE
        );
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, Activator.getBundleMessages()
            .get("error.action", "action", BEGIN_LISTMESSAGE.getIdentifier(), "caller", caller.getIdentifier()), ex);
        return null;
      }
    }
    //3. check messages
    String id = Activator.getServices().getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID();
    EditableMessage m = new EditableMessageImpl(
        id,
        caller.getAgentIdentifier(),
        MessageTypes.BULK,
        listid
    );
    //track message
    m_MessageTracker.put(id, m);
    return m;
  }//beginListMessage

  public EditableMessage beginListMessage(Agent caller, String listid, AgentIdentifier from)
      throws SecurityException, NoSuchListException, PostOfficeServiceException {
    //1. check if list exists
    if (!m_ListManager.existsList(listid)) {
      throw new NoSuchListException(listid);
    }
    try {
      m_PolicyProxy.ensureAuthorization(
          m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller),
          BEGIN_LISTMESSAGE
      );
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages()
          .get("error.action", "action", BEGIN_LISTMESSAGE.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return null;
    }
    //3. check messages
    String id = Activator.getServices().getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID();
    EditableMessage m = new EditableMessageImpl(
        id,
        from,
        MessageTypes.BULK,
        listid
    );
    //track message
    m_MessageTracker.put(id, m);
    return m;

  }//beginListMessage

  public void cancelMessage(Agent caller, EditableMessage emsg)
      throws SecurityException, NoSuchMessageException {
    //1. authentic
    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    //2. tracker
    EditableMessage m = m_MessageTracker.remove(emsg.getIdentifier());
    if (m == null) {
      throw new NoSuchMessageException(emsg.getIdentifier());
    }
    if (m != emsg) {
      throw new SecurityException();
    }
  }//cancelMessage

  public void depositMessage(Agent caller, EditableMessage emsg)
      throws SecurityException, NoSuchMessageException, PostOfficeServiceException {
    //Note: The != comparison is ok here, because it should be really the same object
    // not just an object equal in characteristics.

    if (!m_MessageTracker.containsKey(emsg.getIdentifier()) ||
        m_MessageTracker.remove(emsg.getIdentifier()) != emsg) {
      throw new NoSuchMessageException();
    }

    //1. cast (message is from this bundle, so this cannot fail)
    final EditableMessageImpl msg = (EditableMessageImpl) emsg;
    final boolean depositsent = !(caller instanceof ServiceAgent);
    //2. check for content, otherwise don't send
    if (!msg.hasBody()) {
      throw new PostOfficeServiceException();
    }
    ExecutionService es = Activator.getServices().getExecutionService(ServiceMediator.NO_WAIT);
    if (es == null) {
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("PostOfficeServiceImpl.reference", "service", "ExecutionService"));
    }

    if (msg.isListMessage()) {
      //List mechanism
      final MailingList ml = m_ListManager.getList(msg.getVia());
      //deferred mailing
      es.execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
        public void run() {

          if(depositsent) {
            //1. add to sender
            try {
              m_MessageManager.addMessage(msg.toSentMessage());
            } catch (Exception ex) {
              Activator.log().error(m_LogMarker, "depositMessage()", ex);
            }
          }
          //2. add to recipients
          for (AgentIdentifier to : ml.getRecipients()) {
            POBox pb;
            try {
              //Make sure POBox exists
              pb = m_POBoxManager.getPOBox(to);
            } catch (Exception ex) {
              continue;
            }
            EditableMessageImpl emi = msg.toDepositMessage(to);
            try {
              m_MessageManager.addMessage(emi);
            } catch (Exception ex) {
              Activator.log().error(m_LogMarker, "depositMessage()", ex);
              continue;
            }

            //forward if required
            if (pb.isForwarding()) {
              forwardMessage(emi);
            }

            //Notify if present
            if (pb.isNotify()) {
              m_NotificationManager.notify(emi);
            }
          }//for
        }//run
      });
    } else {
      //Make sure POBox exists
      final POBox pb = m_POBoxManager.getPOBox(msg.getTo());

      es.execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
        public void run() {
          try {
            //1. Add to recipient
            m_MessageManager.addMessage(msg);

            if(depositsent) {
              //2. Add to sender
              m_MessageManager.addMessage(msg.toSentMessage());
            }
            //3. Forward if req.
            if (pb.isForwarding()) {
              forwardMessage(msg);
            }

            //4. Notify if present
            if (pb.isNotify()) {
              m_NotificationManager.notify(msg);
            }

          } catch (Exception ex) {
            Activator.log().error(m_LogMarker, "depositMessage()", ex);
          }
        }//run
      });
    }
  }//depositMessage

  private void forwardMessage(EditableMessage msg) {
    UserdataService uds =
        Activator.getServices().getUserdataService(ServiceMediator.NO_WAIT);
    MailService ms =
        Activator.getServices().getMailService(ServiceMediator.NO_WAIT);
    TransformationService ts = Activator.getServices().getTransformationService(ServiceMediator.NO_WAIT);

    if (uds == null || ms == null || ts == null) {
      Activator.log().error(Activator.getBundleMessages().get("PostOfficeServiceImpl.reference", "service", "UserdataService,MailService,TransformationService"));
      return;
    }
    String recipient;
    String fn;
    try {
      recipient = uds.getEmail(m_ServiceAgentProxy.getAuthenticPeer(), msg.getTo());
      fn = uds.getNickname(m_ServiceAgentProxy.getAuthenticPeer(), msg.getTo());
    } catch (Exception ex) {
      //log, no such email or nick?
      return;
    }
    String subject = msg.getSubject() + " [" + msg.getFrom() + "]";
    //formatting
    String body = msg.getMessageBody().getContent();
    String bodyformat = msg.getBodyFormat();
    if (bodyformat != null && !bodyformat.equalsIgnoreCase("none") && bodyformat.length() > 0) {
      try {
        body = ts.transform(msg.getBodyFormat(), bodyformat, body);
      } catch (TransformationException e) {
        Activator.log().error("forwardMessage()", e);
        //body unaltered.
      }
    }
    try {
      ms.send(
          m_ServiceAgentProxy.getAuthenticPeer(),
          recipient,
          fn,
          subject,
          body
      );
    } catch (MailServiceException e) {
      Activator.log().error("forwardMessage()", e);
    }
  }//forwardMessage

  public void rebuildPOBoxIndex(Agent caller, final AgentIdentifier aid)
      throws SecurityException, NoSuchPOBoxException, PostOfficeServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(
          m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller),
          REBUILD_POBOX_INDEX);
    } catch (NoSuchActionException ex) {
      Activator.log().error(
          m_LogMarker,
          Activator.getBundleMessages().get(
              "error.action",
              "action",
              REBUILD_POBOX_INDEX.getIdentifier(),
              "caller",
              caller.getIdentifier()
          ),
          ex
      );
      return;
    }

    if (!m_POBoxManager.existsPOBox(aid)) {
      throw new NoSuchPOBoxException(aid.getIdentifier());
    }
    ExecutionService es = Activator.getServices().getExecutionService(ServiceMediator.NO_WAIT);
    if (es == null) {
      throw new PostOfficeServiceException(
          Activator.getBundleMessages().get(
              "PostOfficeServiceImpl.reference",
              "service",
              "ExecutionService"
          )
      );
    }
    es.execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {
          m_POBoxManager.getPOBox(aid).rebuildIndex();
        } catch (PostOfficeServiceException e) {
          Activator.log().error("rebuildPOBoxIndex()", e);
        }
      }
    });
  }//rebuildPOBoxIndex

  public void doMaintenance(Agent caller) throws SecurityException, MaintenanceException {
    //1. Security
    try {
      m_PolicyProxy.ensureAuthorization(
          m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), Maintainable.DO_MAINTENANCE
      );
    } catch (NoSuchActionException ex) {
      Activator.log().error(
          m_LogMarker,
          Activator.getBundleMessages().get(
              "error.action",
              "action",
              Maintainable.DO_MAINTENANCE.getIdentifier(),
              "caller",
              caller.getIdentifier()
          ),
          ex
      );
      return;
    }

    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("maintenance.start"));

    //maintain store
    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("maintenance.do.store"));
    m_Store.maintain(m_ServiceAgentProxy);

    //clear trackers; this means that actually happening transactions will fail
    //however, it will remove any possibly existing stale transactions.
    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("maintenance.do.trackers"));
    m_MessageTracker.clear();

    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("maintenance.end"));
  }//maintenance

  private static Action CREATE_POSTBOX = new Action("createPostBox");
  private static Action GET_POSTBOX = new Action("getPostBox");
  private static Action REMOVE_POSTBOX = new Action("removePostBox");

  private static Action CREATE_LIST = new Action("createList");
  private static Action REMOVE_LIST = new Action("removeList");
  private static Action GET_LIST = new Action("getList");
  private static Action GET_LISTS = new Action("getLists");
  private static Action GET_LIST_MEMBERSHIPS = new Action("getListMemberships");

  private static Action BEGIN_MESSAGE = new Action("beginMessage");
  private static Action BEGIN_LISTMESSAGE = new Action("beginListMessage");

  private static Action REBUILD_POBOX_INDEX = new Action("rebuildPOBoxIndex");

  private static Action[] ACTIONS = {
      CREATE_POSTBOX,
      GET_POSTBOX,
      REMOVE_POSTBOX,
      CREATE_LIST,
      REMOVE_LIST,
      GET_LIST,
      GET_LISTS,
      GET_LIST_MEMBERSHIPS,
      BEGIN_MESSAGE,
      BEGIN_LISTMESSAGE,
      REBUILD_POBOX_INDEX,
      Maintainable.DO_MAINTENANCE,
      Restoreable.DO_BACKUP,
      Restoreable.DO_RESTORE
  };

}//class PostOfficeServiceImpl
