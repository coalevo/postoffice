/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.foundation.util.DummyMessages;
import net.coalevo.logging.model.LogProxy;
import net.coalevo.postoffice.service.PostOfficeConfiguration;
import net.coalevo.postoffice.service.PostOfficeService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;

/**
 * This class implements the bundle's activator.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static LogProxy c_Log;
  private static Marker c_LogMarker;

  private static Messages c_BundleMessages = new DummyMessages();
  private static ServiceMediator c_Services;
  private static BundleConfiguration c_BundleConfiguration;

  private static PostOfficeStore c_PostOfficeStore;
  private static PostOfficeService c_PostOfficeService;

  private static File c_IndexStore;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext) throws Exception {
    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }    
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {

              //1. Log
              c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
              c_Log = new LogProxy();
              c_Log.activate(bundleContext);

              //2. Services
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);
              c_BundleMessages =
                  c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                      .getBundleMessages(bundleContext.getBundle());

              //3. BundleConfiguration
              c_BundleConfiguration = new BundleConfiguration(PostOfficeConfiguration.class.getName());
              c_BundleConfiguration.activate(bundleContext);
              c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

              //4. Store and Index
              c_PostOfficeStore = new PostOfficeStore();
              c_PostOfficeStore.activate();
              c_IndexStore = new File(bundleContext.getDataFile(""), "pidx_store");

              //5. PostOfficeService
              c_PostOfficeService = new PostOfficeServiceImpl(c_PostOfficeStore);
              if (!c_PostOfficeService.activate(bundleContext)) {
                log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "PostOfficeService"));
              }
              String[] classes = {PostOfficeService.class.getName(), Maintainable.class.getName()};
              bundleContext.registerService(
                  classes,
                  c_PostOfficeService,
                  null
              );
              log().debug(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "PostOfficeService"));


            } catch (Exception ex) {
              log().error(c_LogMarker, "start(BundleContext)", ex);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {

    //wait start
    if(m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }

    if (c_PostOfficeService != null) {
      c_PostOfficeService.deactivate();
      c_PostOfficeService = null;
    }
    if (c_PostOfficeStore != null) {
      c_PostOfficeStore.deactivate();
      c_PostOfficeStore = null;
    }
    if (c_BundleConfiguration != null) {
      c_BundleConfiguration.deactivate();
      c_BundleConfiguration = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }

    if(c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }
    
    c_IndexStore = null;
    c_BundleMessages = null;
    c_LogMarker = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

  public static File getIndexDir(String indexname) {
    return new File(c_IndexStore, indexname);
  }//getIndexDir

}//class Activator
