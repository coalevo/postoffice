/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import org.apache.commons.collections.list.CursorableLinkedList;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Provides a list of {@link AgentIdentifier} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BlacklistImpl {

  private Marker m_LogMarker = MarkerFactory.getMarker(BlacklistImpl.class.getName());
  private AgentIdentifier m_Owner;
  private Map<String, String> m_Params;
  private CursorableLinkedList m_AgentIdentifiers;
  private PostOfficeStore m_Store;

  public BlacklistImpl(PostOfficeStore store, AgentIdentifier owner) {
    m_Store = store;
    m_Owner = owner;
    prepareList();
  }//constructor

  public AgentIdentifier getOwner() {
    return m_Owner;
  }//getOwner

  public boolean add(AgentIdentifier aid) {
    if (!m_AgentIdentifiers.contains(aid) && !m_Owner.equals(aid)) {
      //Update list
      m_AgentIdentifiers.add(aid);
      //update database
      LibraryConnection lc = null;
      synchronized (m_Params) {
        m_Params.put("agent_id", aid.getIdentifier());
        try {
          lc = m_Store.leaseConnection();
          lc.executeUpdate(ADD, m_Params);
          return true;
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "add()", ex);
        } finally {
          m_Store.releaseConnection(lc);
          m_Params.remove("agent_id");
        }
      }
    }
    return false;
  }//add

  public boolean remove(AgentIdentifier aid) {
    if (m_AgentIdentifiers.contains(aid)) {
      //update list
      m_AgentIdentifiers.remove(aid);
      //update database
      LibraryConnection lc = null;
      synchronized (m_Params) {
        m_Params.put("agent_id", aid.getIdentifier());
        try {
          lc = m_Store.leaseConnection();
          lc.executeUpdate(REMOVE, m_Params);
          return true;
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "remove()", ex);
        } finally {
          m_Store.releaseConnection(lc);
          m_Params.remove("agent_id");
        }
      }
    }
    return false;
  }//remove

  public void clear() {
    for (Iterator iterator = m_AgentIdentifiers.listIterator(); iterator.hasNext();) {
      AgentIdentifier agentIdentifier = (AgentIdentifier) iterator.next();
      remove(agentIdentifier);
    }
  }//clear

  public boolean isEmpty() {
    return m_AgentIdentifiers.isEmpty();
  }//isEmtpy

  public boolean contains(AgentIdentifier aid) {
    return m_AgentIdentifiers.contains(aid);
  }//contains

  public Iterator iterator() {
    return m_AgentIdentifiers.listIterator();
  }//iterator

  public String toString() {
    return m_AgentIdentifiers.toString();
  }//toString

  public int size() {
    return m_AgentIdentifiers.size();
  }//size

  private void refresh() {
    m_AgentIdentifiers.clear();
    LibraryConnection lc = null;
    ResultSet rs = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery(LIST, m_Params);
      while (rs.next()) {
        m_AgentIdentifiers.addLast(m_Store.getAgentIdentifier(rs.getString(1)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "refresh()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//refresh

  private void prepareList() {
    m_AgentIdentifiers = new CursorableLinkedList();
    m_Params = new HashMap<String, String>();
    m_Params.put("pobox_id", m_Owner.getIdentifier());
    refresh();
  }//prepareList

  private static String ADD = "addBlacklisted";
  private static String REMOVE = "removeBlacklisted";
  private static String LIST = "getBlacklisted";

}//class BlacklistImpl
