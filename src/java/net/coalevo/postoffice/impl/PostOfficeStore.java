/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.AgentIdentifierInstanceCache;
import net.coalevo.foundation.model.MaintenanceException;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.postoffice.service.PostOfficeConfiguration;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.userdata.service.UserdataService;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class implements the post office store.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PostOfficeStore implements ConfigurationUpdateHandler {

  private Marker c_LogMarker = MarkerFactory.getMarker(PostOfficeStore.class.getName());
  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;
  private boolean m_New = false;

  //Concurrency handling for backup/restore
  private AtomicInteger m_NumLeased = new AtomicInteger(0);
  private CountDownLatch m_LeaseLatch;
  private CountDownLatch m_RestoreLatch;

  private Messages m_BundleMessages;

  //Managers
  private MessageManager m_MessageManager;
  private POBoxManager m_POBoxManager;
  private ListManager m_ListManager;

  //Caches
  private AgentIdentifierInstanceCache m_AgentIdentifierCache;

  public PostOfficeStore() {

  }//constructor

  public boolean isNew() {
    return m_New;
  }//isNew

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    //If a lease latch is set, wait for countdown
    if (m_LeaseLatch != null) {
      m_LeaseLatch.await();
    }
    LibraryConnection lc = (LibraryConnection) m_ConnectionPool.borrowObject();
    m_NumLeased.addAndGet(1);
    return lc;
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      m_ConnectionPool.returnObject(lc);
      if (m_NumLeased.decrementAndGet() == 0 && m_RestoreLatch != null) {
        m_RestoreLatch.countDown();
      }
    } catch (Exception e) {
      Activator.log().error(c_LogMarker, "releaseConnection()", e);
    }
  }//releaseConnection

  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSchema", null);
      } catch (SQLException ex) {
        m_New = true;
        createSchema(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private boolean createSchema(LibraryConnection lc) {
    try {
      lc = leaseConnection();
      //1. Create schema, tables and index
      lc.executeCreate("createPostOfficeSchema");
      lc.executeCreate("createPOBoxTable");
      lc.executeCreate("createBlacklistTable");
      lc.executeCreate("createMessageDescriptorTable");
      lc.executeCreate("createMessageBodyTable");
      lc.executeCreate("createMailingListTable");
      lc.executeCreate("createMailingListRecipientsTable");
      lc.executeCreate("createDepositedIndex");
      Activator.log().info(c_LogMarker, m_BundleMessages.get("PostOfficeStore.database.schema"));
      return true;
    } catch (Exception ex) {
      Activator.log().error("createSchema()", ex);
    } finally {
      releaseConnection(lc);
    }
    return false;
  }//createSchema


  public boolean activate() {
    m_BundleMessages = Activator.getBundleMessages();
    //1. Configure from persistent configuration
    String ds = "default";
    int cpoolsize = 5;
    int aidcachesize = 50;
    int poboxcachesize = 50;
    int msgcachesize = 100;
    int listcachesize = 25;
    int memcachesize = 50;
    int noucachesize = 50;

    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();

    //Retrieve config
    try {
      ds = mtd.getString(PostOfficeConfiguration.DATA_SOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.DATA_SOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(PostOfficeConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      aidcachesize = mtd.getInteger(PostOfficeConfiguration.AID_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.AID_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      poboxcachesize = mtd.getInteger(PostOfficeConfiguration.POBOX_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.POBOX_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      msgcachesize = mtd.getInteger(PostOfficeConfiguration.MESSAGE_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.MESSAGE_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      listcachesize = mtd.getInteger(PostOfficeConfiguration.LIST_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.LIST_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      memcachesize = mtd.getInteger(PostOfficeConfiguration.MEMBERSHIPS_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.MEMBERSHIPS_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      noucachesize = mtd.getInteger(PostOfficeConfiguration.NOUNREAD_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.NOUNREAD_CACHE_SIZE_KEY),
          ex
      );
    }
    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //prepare SQL library and datasource
    try {
      m_SQLLibrary =
          new Library(PostOfficeStore.class, "net/coalevo/postoffice/impl/postofficestore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "activate()", ex);
      return false;
    }

    DataSourceService dss =
        Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);
    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("error.datasource", "ds", ds),
          nse
      );
      return false;
    }

    //prepare store
    prepareDataSource();
    Activator.log().info(c_LogMarker,
        m_BundleMessages.get("PostOfficeStore.database.info", "source", m_DataSource.toString())
    );

    //Create Caches
    m_AgentIdentifierCache = new AgentIdentifierInstanceCache(aidcachesize);

    //Create Managers
    m_POBoxManager = new POBoxManager(this, poboxcachesize);
    m_MessageManager = new MessageManager(this, msgcachesize, noucachesize);
    m_ListManager = new ListManager(this, listcachesize, memcachesize);

    //add handler config updates
    cm.addUpdateHandler(this);

    return true;
  }//activate

  public synchronized boolean deactivate() {

    //sync and clear caches
    if (m_POBoxManager != null) {
      //will sync
      m_POBoxManager.maintain();
    }

    //1. close all leased connections
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
      } catch (Exception e) {
        Activator.log().error(c_LogMarker, "deactivate()", e);
      }

    }

    m_AgentIdentifierCache = null;
    m_POBoxManager = null;
    m_ListManager = null;
    m_MessageManager = null;

    m_DataSource = null;
    m_SQLLibrary = null;
    m_ConnectionPool = null;
    m_BundleMessages = null;
    return true;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {

    int cpoolsize = 5;
    int aidcachesize = 50;
    int poboxcachesize = 50;
    int msgcachesize = 100;
    int listcachesize = 25;
    int memcachesize = 50;
    int noucachesize = 50;

    //Retrieve config
    try {
      cpoolsize = mtd.getInteger(PostOfficeConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      aidcachesize = mtd.getInteger(PostOfficeConfiguration.AID_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.AID_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      poboxcachesize = mtd.getInteger(PostOfficeConfiguration.POBOX_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.POBOX_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      msgcachesize = mtd.getInteger(PostOfficeConfiguration.MESSAGE_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.MESSAGE_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      listcachesize = mtd.getInteger(PostOfficeConfiguration.LIST_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.LIST_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      memcachesize = mtd.getInteger(PostOfficeConfiguration.MEMBERSHIPS_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.MEMBERSHIPS_CACHE_SIZE_KEY),
          ex
      );
    }
    try {
      noucachesize = mtd.getInteger(PostOfficeConfiguration.NOUNREAD_CACHE_SIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("PostOfficeStore.config.exception", "attribute", PostOfficeConfiguration.NOUNREAD_CACHE_SIZE_KEY),
          ex
      );
    }
    //Update settings
    m_ConnectionPool.setMaxActive(cpoolsize);
    m_ConnectionPool.setMaxIdle(cpoolsize);

    m_AgentIdentifierCache.setCeiling(aidcachesize);
    m_POBoxManager.setPOBoxCacheCeiling(poboxcachesize);
    m_MessageManager.setMessageCacheCeiling(msgcachesize);
    m_MessageManager.setNoUnreadMessageCacheCeiling(noucachesize);
    m_ListManager.setListsCacheCeiling(listcachesize);
    m_ListManager.setMembershipsCacheCeiling(memcachesize);
  }//update

  //*** Caches and Managers ***//

  public AgentIdentifier getAgentIdentifier(String aid) {
    return m_AgentIdentifierCache.get(aid);
  }//getAgentIdentifier


  public AgentIdentifierInstanceCache getAgentIdentifierCache() {
    return m_AgentIdentifierCache;
  }//getAgentIdentifierCache

  public MessageManager getMessageManager() {
    return m_MessageManager;
  }//getMessageManager

  public POBoxManager getPOBoxManager() {
    return m_POBoxManager;
  }//getPOBoxManager

  public ListManager getListManager() {
    return m_ListManager;
  }//getListManager

  public void maintain(ServiceAgentProxy sap) throws MaintenanceException {

    UserdataService uds = Activator.getServices().getUserdataService(ServiceMediator.NO_WAIT);
    if (uds == null) {
      Activator.log().error(
          c_LogMarker, m_BundleMessages.get("maintenance.error.service", "service", "UserdataService")
      );
      return;
    }

    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    LibraryConnection lc = null;

    try {

      lc = leaseConnection();

      //Clean up postboxes for deleted agents and rebuild indices
      rs = lc.executeQuery("listPOBoxes", null);
      while (rs.next()) {
        String aid = rs.getString(1);
        AgentIdentifier a_id = new AgentIdentifier(aid);
        //1. remove deleted
        if (!uds.isUserdataAvailable(sap.getAuthenticPeer(), a_id)) {
          //Purge if no userdata available
          try {
            m_POBoxManager.removePOBox(a_id);
            Activator.log().info(
                c_LogMarker,
                Activator.getBundleMessages().get(
                    "maintenance.store.removedpobox",
                    "pbox",
                    params.get("pobox_id")
                )
            );
          } catch (Exception ex) {
            Activator.log().error(
                c_LogMarker,
                Activator.getBundleMessages().get(
                    "maintenance.error.removedpobox",
                    "pbox",
                    params.get("pobox_id")
                )
            );
          }
        } else {
          //2.rebuild index if outdated
          m_POBoxManager.getPOBox(a_id).rebuildIndex();
          Activator.log().info(
              c_LogMarker,
              Activator.getBundleMessages().get(
                  "maintenance.store.indexed",
                  "pbox",
                  aid
              )
          );
        }
      }


    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, m_BundleMessages.get("maintenance.error.store"), ex);
    } finally {
      SqlUtils.close(rs);
      releaseConnection(lc);
    }

    //clear caches
    Activator.log().info(c_LogMarker, m_BundleMessages.get("maintenance.store.caches"));
    m_AgentIdentifierCache.clear();

    //clear and maintain all managers
    Activator.log().info(c_LogMarker, m_BundleMessages.get("maintenance.store.sync"));
    m_MessageManager.clearCaches();
    m_POBoxManager.maintain();
    m_ListManager.maintain();

  }//maintain

  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate", null);
      } catch (Exception ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory

}//class PostOfficeStore
