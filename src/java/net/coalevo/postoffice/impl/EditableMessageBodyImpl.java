/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.postoffice.model.EditableMessageBody;

/**
 * This class implements {@link EditableMessageBody}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EditableMessageBodyImpl
    extends MessageBodyImpl
    implements EditableMessageBody {

  public EditableMessageBodyImpl() {
  }//constructor

  public EditableMessageBodyImpl(String str) {
    m_Content = str;
  }//constructor

  public void setContent(String content) {
    m_Content = content;
  }//setContent


  public MessageBodyImpl toMessageBody() {
    return new MessageBodyImpl(m_Content);
  }//toContent

}//class EditableMessageBodyImpl
