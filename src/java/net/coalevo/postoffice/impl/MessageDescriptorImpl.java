/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.BaseIdentifiable;
import net.coalevo.postoffice.model.MessageDescriptor;
import net.coalevo.postoffice.model.MessageType;

/**
 * This class implements {@link MessageDescriptor}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessageDescriptorImpl
    extends BaseIdentifiable
    implements MessageDescriptor {

  protected String m_Ancestor;
  protected MessageType m_Type;
  protected long m_Timestamp;
  protected boolean m_Read;
  protected AgentIdentifier m_Owner;
  protected AgentIdentifier m_From;
  protected AgentIdentifier m_To;
  protected String m_Via;
  protected String m_Subject;
  protected String m_BodyFormat;

  protected MessageDescriptorImpl(String id) {
    super(id);
  }//constructor

  public MessageDescriptorImpl(String id, String ancestor, AgentIdentifier owner, MessageType type, long timestamp, boolean read, AgentIdentifier from, AgentIdentifier to, String via, String subject, String bodyFormat) {
    super(id);
    m_Ancestor = ancestor;
    m_Owner = owner;
    m_Type = type;
    m_Timestamp = timestamp;
    m_Read = read;
    m_From = from;
    m_To = to;
    m_Via = via;
    m_Subject = subject;
    m_BodyFormat = bodyFormat;
  }//constructor

  public AgentIdentifier getOwner() {
    return m_Owner;
  }//getOwner

  public String getAncestor() {
    return m_Ancestor;
  }//getAncestor

  public MessageType getMessageType() {
    return m_Type;
  }//getMessageType

  public long getTimestamp() {
    return m_Timestamp;
  }//getTimestamp

  public boolean isRead() {
    return m_Read;
  }//isRead

  public AgentIdentifier getFrom() {
    return m_From;
  }//getFrom

  public AgentIdentifier getTo() {
    return m_To;
  }//getTo

  public String getVia() {
    return m_Via;
  }//getVia

  public String getSubject() {
    return m_Subject;
  }//getSubject

  public String getBodyFormat() {
    return m_BodyFormat;
  }//getBodyFormat

  public boolean isReceived() {
    return m_Owner.equals(m_To);
  }//isReceived

  public boolean isListMessage() {
    return m_Via != null;
  }//isListMessage

  public void markRead() {
    m_Read = true;
  }//markRead

}//class MessageDescriptorImpl
