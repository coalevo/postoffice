/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.postoffice.model.MessageTypes;
import net.coalevo.postoffice.model.NoSuchMessageException;
import net.coalevo.postoffice.model.POBox;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class implements a manager for messages in POBoxes
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessageManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(MessageManager.class.getName());

  private PostOfficeStore m_Store;
  //Caches
  private LRUCacheMap<String, MessageDescriptorImpl> m_MessageDescriptorCache;
  //New Message Cache
  private LRUCacheMap<AgentIdentifier, Boolean> m_NoUnreadMessageCache;

  public MessageManager(PostOfficeStore store, int mdcs, int nocs) {
    m_Store = store;
    m_MessageDescriptorCache = new LRUCacheMap<String, MessageDescriptorImpl>(mdcs);
    m_NoUnreadMessageCache = new LRUCacheMap<AgentIdentifier, Boolean>(nocs);
  }//MessageManager

  public void setMessageCacheCeiling(int size) {
    m_MessageDescriptorCache.setCeiling(size);
  }//setMessageCacheCeiling

  public void setNoUnreadMessageCacheCeiling(int size) {
    m_NoUnreadMessageCache.setCeiling(size);
  }//setNoUnreadMessageCacheCeiling

  public void clearCaches() {
    m_MessageDescriptorCache.clear();
    m_NoUnreadMessageCache.clear();
  }//clearCaches

  public void addMessage(EditableMessageImpl md) throws PostOfficeServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", md.getOwner().getIdentifier());
    params.put("msg_id", md.getIdentifier());
    params.put("ancestor", md.getAncestor()); //allowed to be null
    params.put("mtype", md.getMessageType().getIdentifier());
    params.put("deposited", Long.toString(md.timestampDeposit()));
    params.put("read", (md.isRead()) ? "1" : "0");
    params.put("from", md.getFrom().getIdentifier());
    if (md.getTo() != null) {
      params.put("to", md.getTo().getIdentifier());
    } else {
      params.put("to", null);
    }
    params.put("via", md.getVia());  //std msg should be null
    params.put("subject", md.getSubject());
    params.put("format", md.getBodyFormat());

    HashMap<String, String> params2 = new HashMap<String, String>();
    params2.put("pobox_id", md.getOwner().getIdentifier());
    params2.put("msg_id", md.getIdentifier());
    params2.put("document", md.getMessageBody().getContent());

//    System.out.println(" ======> " + params.toString());
    LibraryConnection lc = null;
    try {
      try {
        lc = m_Store.leaseConnection();
        lc.executeUpdate("addMessage", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "addMessage()", ex);
        throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.addfailed"), ex);
      }
      try {
        lc.executeUpdate("addMessageBody", params2);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "addMessage()", ex);
        //rollback, albeit will cause two connections to be used temporarily
        removeMessage(md.getTo().getIdentifier(), md.getIdentifier());
        throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.addfailed"), ex);
      }
      if (!md.isRead()) {
        m_NoUnreadMessageCache.remove(md.getOwner());
      }
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//addMessage

  public MessageDescriptorImpl getMessage(POBox box, String id)
      throws PostOfficeServiceException, NoSuchMessageException {

    MessageDescriptorImpl md = m_MessageDescriptorCache.get(id);
    if (md == null) {
      //load from RDBMS
      //Build list
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("pobox_id", box.getOwner().getIdentifier());
      params.put("msg_id", id);

      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getMessage", params);
        if (rs.next()) {
          md = fromResult(box, rs);
          m_MessageDescriptorCache.put(id, md);
        } else {
          throw new NoSuchMessageException(id);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "getMessage()", ex);
        throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.getfailed"), ex);
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
      }
    }

    return md;
  }//getMessage

  public void removeMessage(POBox box, String msgid) throws PostOfficeServiceException {
    removeMessage(box.getOwner().getIdentifier(), msgid);
  }//removeMessage

  private void removeMessage(String pbid, String msgid) throws PostOfficeServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", pbid);
    params.put("msg_id", msgid);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("removeMessage", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "removeMessage()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.removefailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    m_MessageDescriptorCache.remove(msgid);
  }//removeMessage

  public void markRead(POBox box, String msgid) throws PostOfficeServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", box.getOwner().getIdentifier());
    params.put("msg_id", msgid);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("markRead", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "markRead()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.markreadfailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    //flush from cache
    m_MessageDescriptorCache.remove(msgid);
  }//markRead

  public List<String> listMessages(POBox box)
      throws PostOfficeServiceException {

    //load from RDBMS
    ArrayList<String> msgs = new ArrayList<String>();
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", box.getOwner().getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listMessages", params);
      while (rs.next()) {
        msgs.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listMessages()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.listmsgfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }

    return msgs;
  }//listMessages

  public List<String> listUnread(POBox box)
      throws PostOfficeServiceException {

    //load from RDBMS
    ArrayList<String> msgs = new ArrayList<String>();
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", box.getOwner().getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listUnread", params);
      while (rs.next()) {
        msgs.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listUnread()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.listunreadfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }

    return msgs;
  }//listUnread

  public boolean hasUnread(POBox box)
      throws PostOfficeServiceException {

    if (m_NoUnreadMessageCache.containsKey(box.getOwner())) {
      return false; //no unread message
    }
    //load from RDBMS
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", box.getOwner().getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("hasUnread", params);
      boolean b = (rs.next() && rs.getInt(1) > 0);
      if (!b) {
        m_NoUnreadMessageCache.put(box.getOwner(), true); //store no unread message
      }
      return b;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "hasUnread()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.hasunreadfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//hasUnread

  public MessageBodyImpl getMessageBody(POBox box, String msgid)
      throws PostOfficeServiceException {

    MessageBodyImpl mb = null;
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", box.getOwner().getIdentifier());
    params.put("msg_id", msgid);

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getMessageBody", params);
      if (rs.next()) {
        mb = new MessageBodyImpl(rs.getString(1));
      } else {
        throw new NoSuchMessageException(msgid);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getMessageBody()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.getbodyfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return mb;
  }//getMessageBody

  public int getMessageCount(POBox box)
      throws PostOfficeServiceException {

    int count = 0;
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", box.getOwner().getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getMessageCount", params);
      if (rs.next()) {
        count = rs.getInt(1);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getMessageCount()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("MessageManager.getcountfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return count;
  }//getMessageCount

  private MessageDescriptorImpl fromResult(POBox box, ResultSet rs) throws SQLException {
    AgentIdentifier to = null;
    String str = rs.getString(8);
    if(str != null && str.length() > 0) {
      to = m_Store.getAgentIdentifier(str);
    }
    return new MessageDescriptorImpl(
        rs.getString(2),
        rs.getString(3),
        box.getOwner(),
        MessageTypes.get(rs.getString(4)),
        new Long(rs.getLong(5)),
        rs.getShort(6) == 1,
        m_Store.getAgentIdentifier(rs.getString(7)),
        to,
        rs.getString(9),
        rs.getString(10),
        rs.getString(11)
    );
  }//fromResult

}//class MessageManager
