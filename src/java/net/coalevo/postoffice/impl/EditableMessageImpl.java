/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.postoffice.model.EditableMessage;
import net.coalevo.postoffice.model.EditableMessageBody;
import net.coalevo.postoffice.model.MessageType;

/**
 * This class implements an {@link EditableMessage}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EditableMessageImpl
    extends MessageDescriptorImpl
    implements EditableMessage {

  protected EditableMessageBody m_MessageBody;

  protected EditableMessageImpl(String id) {
    super(id);
  }//constructor

  public EditableMessageImpl(String id,
                             AgentIdentifier from,
                             AgentIdentifier to,
                             MessageType type) {
    super(id);
    m_From = from;
    m_To = to;
    m_Owner = to;
    m_Type = type;
  }//EditableMessageImpl

  public EditableMessageImpl(String id, AgentIdentifier from, MessageType type, String listid) {
    super(id);
    m_From = from;
    m_Type = type;
    m_Via = listid;
  }//constructor

  public void setAncestor(String an) {
    m_Ancestor = an;
  }//setAncestor

  public boolean hasAncestor() {
    return m_Ancestor != null && m_Ancestor.length() > 0;
  }//hasAncestor

  public void setTo(AgentIdentifier to) {
    m_To = to;
    m_Owner = to;
  }//setTo

  public void setSubject(String str) {
    m_Subject = str;
  }//setSubject

  public void setBodyFormat(String str) {
    m_BodyFormat = str;
  }//setBodyFormat

  public EditableMessageBody getMessageBody() {
    if (m_MessageBody == null) {
      m_MessageBody = new EditableMessageBodyImpl();
    }
    return m_MessageBody;
  }//getMessageBody

  public boolean hasSubject() {
    return m_Subject != null;
  }//hasSubject

  public boolean hasBody() {
    return (m_MessageBody != null) && (!m_MessageBody.isEmpty());
  }//hasBody

  public void setOwner(AgentIdentifier owner) {
    m_Owner = owner;
  }//setOwner

  public long timestampDeposit() {
    m_Timestamp = System.currentTimeMillis();
    return m_Timestamp;
  }//timestampDeposit

  public EditableMessageImpl toSentMessage() {
    EditableMessageImpl emi = new EditableMessageImpl(
        Activator.getServices().getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID()
    );
    emi.m_Ancestor = this.m_Ancestor;
    emi.m_Type = this.m_Type;
    emi.m_Owner = this.m_From;
    emi.m_From = this.m_From;
    emi.m_To = this.m_To;
    emi.m_Via = this.m_Via;
    emi.m_Timestamp = this.m_Timestamp;
    emi.m_Read = true;
    emi.m_BodyFormat = this.m_BodyFormat;
    emi.m_Subject = this.m_Subject;
    emi.m_MessageBody = this.m_MessageBody;
    return emi;
  }//toSentMessage

  public EditableMessageImpl toDepositMessage(AgentIdentifier to) {
    EditableMessageImpl emi = new EditableMessageImpl(
        Activator.getServices().getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID()
    );
    emi.m_Ancestor = this.m_Ancestor;
    emi.m_Type = this.m_Type;
    emi.m_Owner = to;
    emi.m_From = this.m_From;
    emi.m_To = to;
    emi.m_Via = this.m_Via;
    emi.m_Timestamp = this.m_Timestamp;
    emi.m_Read = false;
    emi.m_BodyFormat = this.m_BodyFormat;
    emi.m_Subject = this.m_Subject;
    emi.m_MessageBody = this.m_MessageBody;
    return emi;
  }//toDepositMessage

}//class EditableMessageImpl
