/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.postoffice.model.MessageBody;

import java.io.*;

/**
 * This class implements {@link MessageBody}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessageBodyImpl implements MessageBody {

  protected String m_Content = "";

  public MessageBodyImpl() {

  }//constructor

  public MessageBodyImpl(String str) {
    m_Content = str;
  }//constructor

  public MessageBodyImpl(Reader in) {
    try {
      setContent(in);
    } catch (IOException ex) {

    }
  }//constructor

  public String getContent() {
    return m_Content;
  }//getContent

  public void writeTo(OutputStream out)
      throws IOException {
    out.write(m_Content.getBytes("utf-8"));
    out.flush();
  }//writeTo

  public void setContent(Reader r) throws IOException {
    BufferedReader br = new BufferedReader(r);
    StringBuilder sbuf = new StringBuilder();

    String buf = null;
    do {
      buf = br.readLine();
      sbuf.append(buf);
    } while (buf != null);
    m_Content = sbuf.toString();
  }//setContent

  public void setContent(InputStream in)
      throws IOException {

    setContent(new InputStreamReader(in));
  }//setContent

  public boolean isEmpty() {
    return (m_Content == null || m_Content.length() == 0);
  }//isEmpty

  public EditableMessageBodyImpl toEditableMessageBody() {
    return new EditableMessageBodyImpl(m_Content);
  }//toEditableMessageBody

}//class MessageBodyImpl
