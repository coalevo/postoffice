/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.service.UUIDGeneratorService;
import net.coalevo.messaging.model.EditableInteractiveMessage;
import net.coalevo.messaging.model.InteractiveMessageTypes;
import net.coalevo.messaging.model.ServiceMessagingProxy;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.postoffice.model.POBox;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.presence.model.*;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.model.ServiceAgentProxy;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.*;

/**
 * Implements a notification manager for presence and messaging based
 * notification.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class NotificationManager {

  private Marker m_LogMarker;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private ServicePresenceProxy m_ServicePresenceProxy;
  private ServiceMessagingProxy m_ServiceMessagingProxy;

  private POBoxManager m_POBoxManager;

  private Set<AgentIdentifier> m_PresentSubscribers;
  private ServiceMediator m_Services;

  public NotificationManager(ServiceAgentProxy sa, POBoxManager pm) {
    m_ServiceAgentProxy = sa;
    m_POBoxManager = pm;
  }//constructor

  public void activate(BundleContext bc) {
    m_LogMarker = MarkerFactory.getMarker(NotificationManager.class.getName());
    m_Services = Activator.getServices();

    m_PresentSubscribers = new HashSet<AgentIdentifier>();

    //1. Presence Handling
    m_ServicePresenceProxy = new ServicePresenceProxy(new NotificationPresenceListener(), m_ServiceAgentProxy, Activator.log());
    m_ServicePresenceProxy.activate(bc);

    //2. Messaging Handling
    m_ServiceMessagingProxy = new ServiceMessagingProxy(m_ServicePresenceProxy, Activator.log());
    m_ServiceMessagingProxy.activate(bc);

    //3. Run presence subscription task on notifying on that were not yet
    //requested.
    final Presence p = m_ServicePresenceProxy.getPresence();
    final PresenceService ps = m_ServicePresenceProxy.getPresenceService();
    m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {
          List<AgentIdentifier> subscribers = m_POBoxManager.listAllNotifying();
          AgentIdentifierList subscribedto = p.getSubscriptionsTo();
          for (Iterator<AgentIdentifier> iterator = subscribers.listIterator(); iterator.hasNext();) {
            AgentIdentifier aid = iterator.next();
            if (!subscribedto.contains(aid)) {
              ps.requestSubscriptionTo(p, aid);
            }
          }
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "activate()", ex);
        }
      }//run
    });
  }//activate

  public void deactivate() {
    if (m_ServiceMessagingProxy != null) {
      m_ServiceMessagingProxy.deactivate();
      m_ServiceMessagingProxy = null;
    }
    if (m_ServicePresenceProxy != null) {
      m_ServicePresenceProxy.deactivate();
      m_ServicePresenceProxy = null;
    }

    m_LogMarker = null;
    m_Services = null;
  }//deactivate

  /**
   * Notify an agent of a deposited message.
   * The postbox was already checked for notified on by the service implementation.
   *
   * @param msg the message to notify about.
   */
  public void notify(final EditableMessageImpl msg) {
    final AgentIdentifier aid = msg.getTo();

    if (!m_PresentSubscribers.contains(aid)) {
      return;
    }

    final MessagingService ms = m_ServiceMessagingProxy.getMessagingService();
    final Presence p = m_ServicePresenceProxy.getPresence();
    final Messages bm = Activator.getBundleMessages();
    final UUIDGeneratorService uuid = m_Services.getUUIDGeneratorService(ServiceMediator.WAIT_UNLIMITED);

    m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {
          //1. Prepare Message
          EditableInteractiveMessage im = ms.create(
              p, InteractiveMessageTypes.NOTIFICATION, uuid.getUID());
          im.setConfirmationRequired(false);

          //2. Get pobox
          POBox pb = m_POBoxManager.getPOBox(aid);
          Locale lang = pb.getLanguage();
          im.setTo(pb.getOwner());
          im.setThread("MAIL");
          im.setSubject(
              bm.get(
                  lang,
                  "NotificationManager.notification.subject",
                  "from",
                  msg.getFrom().getIdentifier()
              )
          );
          im.setBody(
              bm.get(
                  lang,
                  "NotificationManager.notification.body",
                  "subject",
                  msg.getSubject()
              )
          );
          NotificationManager.this.m_ServiceMessagingProxy.getMessagingService().send(p, im);
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "notify()", ex);
        }
      }//run
    });
  }//notify

  /**
   * Enable or disable notification.
   *
   * @param aid a subscription.
   */
  public void ensureSubscription(final AgentIdentifier aid) {

    m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(m_ServiceAgentProxy.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {

          //1. Handle Presence subscription
          Presence p = m_ServicePresenceProxy.getPresence();
          PresenceService ps = m_ServicePresenceProxy.getPresenceService();

          //RequestSubscription if not subscribed
          if (!p.getSubscriptionsTo().contains(aid)) {
            ps.requestSubscriptionTo(p, aid);
          }

        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "activate()", ex);
        }
      }//run
    });
  }//ensureSubscription

  //*** Keep track of presences ***//
  protected void handlePresence(final AgentIdentifier aid) {
    if (m_PresentSubscribers.contains(aid)) {
      return;
    } else {
      m_PresentSubscribers.add(aid);
    }
  }//handlePresence

  protected void handleAbsence(final AgentIdentifier aid) {
    if (!m_PresentSubscribers.remove(aid)) {
      return;
    }
  }//handleAbsence

  protected void handleNoSubscription(final AgentIdentifier aid) {
    //turn off notification in the pobox, this will in turn turn
    //it off here
    try {
      m_POBoxManager.getPOBox(aid).setNotify(false);
    } catch (PostOfficeServiceException e) {
      Activator.log().error("ensureSubscription()", e);
    }
  }//handleNoSubscription

  class NotificationPresenceListener
      extends PresenceServiceListenerAdapter {

    public void found(PresenceProxy p) {
      if (p.isAvailable()) {
        NotificationManager.this.handlePresence(p.getAgentIdentifier());
      }
      if (p.isUnavailable()) {
        NotificationManager.this.handleAbsence(p.getAgentIdentifier());
      }
    }//found

    public void receivedPresence(PresenceProxy p) {
      if (p.isAvailable()) {
        NotificationManager.this.handlePresence(p.getAgentIdentifier());
      }
    }//receivedPresence

    public void becamePresent(PresenceProxy p) {
      if (p.isAvailable()) {
        NotificationManager.this.handlePresence(p.getAgentIdentifier());
      }
    }//becamePresent

    public void becameAbsent(PresenceProxy p) {
      NotificationManager.this.handleAbsence(p.getAgentIdentifier());
    }//becameAbsent

    public void statusUpdated(PresenceProxy p) {
      if (p.isAvailable()) {
        NotificationManager.this.handlePresence(p.getAgentIdentifier());
      }
      if (p.isUnavailable()) {
        NotificationManager.this.handleAbsence(p.getAgentIdentifier());
      }
    }//statusUpdated

    public void subscriptionDenied(AgentIdentifier aid, String reason) {
      NotificationManager.this.handleNoSubscription(aid);
    }//subscriptionDenied

    public void subscriptionCanceled(AgentIdentifier aid) {
      NotificationManager.this.handleNoSubscription(aid);
    }//subscriptionCanceled

    public void requestedSubscription(PresenceProxy p) {
      m_ServicePresenceProxy.getPresenceService().denySubscription(
          m_ServicePresenceProxy.getPresence(), p.getAgentIdentifier(), "service");
    }//requestedSubscription

  }//inner class NotificationPresenceListener

}//class NotificationManager
