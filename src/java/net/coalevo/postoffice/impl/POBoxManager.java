/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.CacheMapExpelHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.postoffice.model.NoSuchPOBoxException;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * This class implements a manger for {@link POBoxImpl} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class POBoxManager {

  private PostOfficeStore m_Store;
  private Marker m_LogMarker = MarkerFactory.getMarker(POBoxManager.class.getName());

  private LRUCacheMap<AgentIdentifier, POBoxImpl> m_POBoxCache;

  public POBoxManager(PostOfficeStore store, int poboxcachesize) {
    m_Store = store;
    m_POBoxCache = new LRUCacheMap<AgentIdentifier, POBoxImpl>(poboxcachesize);
    m_POBoxCache.setExpelHandler(new POBoxCacheExpelHandler());
  }//constructor

  public void setPOBoxCacheCeiling(int pobcs) {
    m_POBoxCache.setCeiling(pobcs);
  }//setPBoxCacheCeiling

  public void maintain() {
    //clear with synch
    m_POBoxCache.clear(true);
  }//maintain

  public POBoxImpl createPOBox(AgentIdentifier owner, Locale lang, boolean forwarding, boolean priv, boolean notify)
      throws PostOfficeServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", owner.getIdentifier());
    params.put("language", lang.getLanguage());
    params.put("forwarding", (forwarding) ? "1" : "0");
    params.put("private", (priv) ? "1" : "0");
    params.put("notify", (notify) ? "1" : "0");
    params.put("idxreb", "0");

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("addPOBox", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "remove()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("POBoxManager.createfailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    return new POBoxImpl(m_Store, owner, lang, forwarding, priv, notify, false);
  }//createPOBox

  public POBoxImpl getPOBox(AgentIdentifier owner)
      throws PostOfficeServiceException, NoSuchPOBoxException {

    POBoxImpl pobi = m_POBoxCache.get(owner);

    if (pobi == null) {
      //Build list
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("pobox_id", owner.getIdentifier());

      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getPOBox", params);
        if (rs.next()) {
          pobi = new POBoxImpl(m_Store,
              owner,
              new Locale(rs.getString(2)), //language
              rs.getShort(3) == 1,
              rs.getShort(4) == 1,
              rs.getShort(5) == 1,
              rs.getShort(6) == 1
          );
          m_POBoxCache.put(owner, pobi);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "getPOBox()", ex);
        throw new PostOfficeServiceException(Activator.getBundleMessages().get("POBoxManager.getfailed"), ex);
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
      }
    }
    if (pobi == null) {
      throw new NoSuchPOBoxException(owner.getIdentifier());
    } else {
      return pobi;
    }
  }//getBOBox

  public boolean existsPOBox(AgentIdentifier to)
      throws PostOfficeServiceException {
    if (m_POBoxCache.containsKey(to)) {
      return true;
    }
    if(to == null) {
      return false;
    }
    //Build list
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", to.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("existsPOBox", params);
      return (rs.next() && rs.getInt(1) > 0);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "existsPOBox()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("POBoxManager.existsfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//existsPOBox

  public boolean removePOBox(AgentIdentifier aid)
      throws PostOfficeServiceException {

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", aid.getIdentifier());

    //Remove from db
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("removePOBox", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "remove()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("POBoxManager.removefailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    //Remove from cache (should not trigger expel handler)
    m_POBoxCache.remove(aid);

    return true;
  }//removePOBox

  public List<AgentIdentifier> listAllNotifying()
      throws PostOfficeServiceException {

    List<AgentIdentifier> ids = new ArrayList<AgentIdentifier>();
    //get from DS
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listAllNotifying", null);
      while (rs.next()) {
        ids.add(m_Store.getAgentIdentifier(rs.getString(1)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listAllNotifying", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("POBoxManager.listallnotifyingfailed"));
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return ids;
  }//listNotifying

  protected void updatePOBox(POBoxImpl pobox)
      throws PostOfficeServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", pobox.getOwner().getIdentifier());
    params.put("language", pobox.getLanguage().getLanguage());
    params.put("forwarding", (pobox.isForwarding()) ? "1" : "0");
    params.put("private", (pobox.isPrivate()) ? "1" : "0");
    params.put("notify", (pobox.isNotify()) ? "1" : "0");
    params.put("idxreb", (pobox.isIndexRebuild()) ? "1" : "0");

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("updatePOBox", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "remove()", ex);
      throw new PostOfficeServiceException(Activator.getBundleMessages().get("POBoxManager.updatefailed"), ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//updatePOBox

  private class POBoxCacheExpelHandler
      implements CacheMapExpelHandler<AgentIdentifier, POBoxImpl> {

    public void expelled(Map.Entry<AgentIdentifier, POBoxImpl> entry) {
      POBoxImpl pobox = entry.getValue();
      if (!pobox.isDirty()) {
        return;
      }
      try {
        POBoxManager.this.updatePOBox(pobox);
        pobox.setDirty(false);
      } catch (PostOfficeServiceException ex) {
        //not much that can be done else than logging (already logged in update method)
      }
    }//expelled
  }//class POBoxCacheExpelHandler

}//class POBoxManager
