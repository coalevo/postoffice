/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.util.crypto.DigestUtil;
import net.coalevo.postoffice.model.*;
import net.coalevo.text.model.TransformationException;
import net.coalevo.text.model.UnsupportedTransformException;
import net.coalevo.text.service.TransformationService;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hit;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * This class provides a Lucene based fulltext index for discussion
 * entries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessageIndex {

  private Marker m_LogMarker = MarkerFactory.getMarker(MessageIndex.class.getName());
  private POBox m_POBox;

  private Directory m_Directory;
  private Analyzer m_Analyzer;
  private QueryParser m_QueryParser;
  private IndexSearcher m_IndexSearcher;

  /**
   * Constructs this <tt>EntryIndex</tt>.
   *
   * @param pobox the {@link POBox} this index is related to.
   */
  public MessageIndex(POBox pobox) {
    m_POBox = pobox;
  }//constructor

  /**
   * Initializes this <tt>MessageIndex</tt>.
   *
   * @throws IOException if the iinitialization fails.
   */
  public void init() throws IOException {
    //prepare analyzer
    prepareAnalyzer();

    String str = DigestUtil.digestToHexString(m_POBox.getOwner().getIdentifier().getBytes("utf-8"));
    //prepare index file path
    final File f = Activator.getIndexDir(str);

    m_Directory = FSDirectory.getDirectory(f, !f.exists());
    m_QueryParser = new QueryParser(PostOfficeTokens.ELEMENT_BODY, m_Analyzer);
  }//init

  public synchronized void rebuildIndex(PostOfficeStore ps)
      throws IOException {
    if (m_Directory == null) {
      init();
    }
    //open the index writer, deleting the old stuff
    IndexWriter iw = new IndexWriter(m_Directory, m_Analyzer, true);
    //TransformationService
    TransformationService transforms = Activator.getServices().getTransformationService(ServiceMediator.NO_WAIT);
    if (transforms == null) {
      return;
    }
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("pobox_id", m_POBox.getOwner().getIdentifier());
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = ps.leaseConnection();
      rs = lc.executeQuery("listMessagesForIndex", params);

      String tmp = "";
      while (rs.next()) {

        try {
          Document d = new Document();

          //1. Message id
          tmp = rs.getString(2);
          addField(d, PostOfficeTokens.ATTR_ID, tmp, true, false);
          //2. Ancestor id
          tmp = rs.getString(3);
          addField(d, PostOfficeTokens.ATTR_ANCESTOR, tmp, true, false);
          //3. Message type
          tmp = rs.getString(4);
          addField(d, PostOfficeTokens.ELEMENT_TYPE, tmp, false, false);
          //4. Deposited
          addDayField(d, PostOfficeTokens.ELEMENT_DEPOSITED, rs.getLong(5));
          //6. Read
          addBoolField(d, PostOfficeTokens.ATTR_READ, rs.getBoolean(6));
          //7. From
          tmp = rs.getString(7);
          addField(d, PostOfficeTokens.ELEMENT_FROM, tmp, false, false);
          //8. To
          tmp = rs.getString(8);
          addField(d, PostOfficeTokens.ELEMENT_TO, tmp, false, false);
          //9. via
          tmp = rs.getString(9);
          addField(d, PostOfficeTokens.ELEMENT_VIA, tmp, false, false);
          //10. subject
          tmp = rs.getString(10);
          addField(d, PostOfficeTokens.ELEMENT_SUBJECT, tmp, false, true);
          //11. Body
          String bodyformat = rs.getString(11);
          tmp = rs.getString(12);
          if (bodyformat != null && bodyformat.length() > 0) {
            try {
              tmp = transforms.transform(bodyformat, "plain", tmp);
            } catch (UnsupportedTransformException ex) {
              Activator.log().error(m_LogMarker, "rebuildIndex()", ex);
            } catch (TransformationException ex) {
              Activator.log().error(m_LogMarker, "rebuildIndex()", ex);
            }
          }
          addField(d, PostOfficeTokens.ELEMENT_BODY, tmp, false, true);

          // Add to index
          bulkAdd(iw, d);
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "rebuildIndex()", ex);
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "rebuildIndex()", ex);
    } finally {
      SqlUtils.close(rs);
      ps.releaseConnection(lc);
    }

    //optimize
    iw.optimize();
    iw.close();

    //invalidate searcher
    m_IndexSearcher = null;
  }//rebuildIndex

  private void addField(Document udd, String name, String txt, boolean store, boolean tokenize) {
    if (txt != null) {
      Field f = new Field(
          name,
          txt,
          (store) ? Field.Store.YES : Field.Store.NO,
          (tokenize) ? Field.Index.TOKENIZED : Field.Index.UN_TOKENIZED
      );
      udd.add(f);
    }
  }//addField

  private void addDayField(Document d, String name, long ts) {
    String tmp = DateTools.timeToString(ts, DateTools.Resolution.DAY);
    d.add(
        new Field(name, tmp, Field.Store.NO, Field.Index.UN_TOKENIZED)
    );
  }//addDayField

  private void addBoolField(Document d, String name, Boolean b) {
    String tmp = b.toString();
    d.add(
        new Field(name, tmp, Field.Store.NO, Field.Index.UN_TOKENIZED)
    );
  }//addBoolField

  public boolean exists() throws IOException {
    return IndexReader.indexExists(m_Directory);
  }//exists

  /**
   * Adds a document for the {@link MessageDescriptor} and the corresponding {@link MessageBody}
   * to this index, using an already open writer.
   * </p>
   *
   * @param iw the IndexWriter.
   * @param d  the Document to be added.
   * @throws IOException if an I/O Operation fails.
   */
  private void bulkAdd(IndexWriter iw, Document d)
      throws IOException {
    iw.addDocument(d);
  }//bulkAdd

  /**
   * Returns an <tt>IndexReader</tt> for the wrapped index.
   *
   * @return an <tt>IndexReader</tt> instance.
   * @throws IOException if an I/O error occurs.
   */
  public IndexReader getReader() throws IOException {
    return IndexReader.open(m_Directory);
  }//getReader

  /**
   * Returns an <tt>IndexSearcher</tt> for the wrapped index.
   *
   * @return an <tt>IndexSearcher</tt> instance.
   * @throws IOException if an I/O error occurs.
   */
  public IndexSearcher getSearcher() throws IOException {
    return new IndexSearcher(m_Directory);
  }//getSearcher

  /**
   * Prepares the analyzer.
   */
  private void prepareAnalyzer() {
    PerFieldAnalyzerWrapper pa = new PerFieldAnalyzerWrapper(
        new StandardAnalyzer());

    //use certain analyzers for specific fields
    pa.addAnalyzer(PostOfficeTokens.ATTR_ID, new KeywordAnalyzer());
    pa.addAnalyzer(PostOfficeTokens.ATTR_ANCESTOR, new KeywordAnalyzer());
    pa.addAnalyzer(PostOfficeTokens.ELEMENT_TYPE, new KeywordAnalyzer());
    pa.addAnalyzer(PostOfficeTokens.ELEMENT_FROM, new KeywordAnalyzer());
    pa.addAnalyzer(PostOfficeTokens.ELEMENT_TO, new KeywordAnalyzer());
    pa.addAnalyzer(PostOfficeTokens.ELEMENT_VIA, new KeywordAnalyzer());

    m_Analyzer = pa;
  }//prepareAnalyzer

  public List<MessageHit> search(String query,
                                 int numhits) {
    List<MessageHit> s = new ArrayList<MessageHit>(numhits);

    try {
      Query q = null;

      synchronized (m_QueryParser) {
        q = m_QueryParser.parse(query);
      }

      if (m_IndexSearcher == null) {
        m_IndexSearcher = getSearcher();
      }
      Hits hits = m_IndexSearcher.search(q);

      //Min of max result number and hits length.
      int to = Math.min(numhits, hits.length());
      for (Iterator iterator = hits.iterator(); iterator.hasNext(); to--) {
        Hit h = (Hit) iterator.next();
        s.add(
            new SimpleMessageHit(
                h.get(PostOfficeTokens.ATTR_ID),
                h.getScore()
            )
        );
        if (to == 0) {
          break;
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "searchEntries()", ex);
    }
    return s;
  }//searchEntries

  static class SimpleMessageHit implements MessageHit {

    private String m_MessageIdentifier;
    private float m_Score;

    public SimpleMessageHit(String mid, float score) {
      m_MessageIdentifier = mid;
      m_Score = score;
    }//constructor

    public String getMessageIdentifier() {
      return m_MessageIdentifier;
    }//getMessageIdentifier

    public float getScore() {
      return m_Score;
    }//getScore

    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof SimpleMessageHit)) return false;

      SimpleMessageHit that = (SimpleMessageHit) o;

      if (m_MessageIdentifier != null ? !m_MessageIdentifier.equals(that.m_MessageIdentifier) : that.m_MessageIdentifier != null)
        return false;

      return true;
    }//equals

    public int hashCode() {
      int result;
      result = (m_MessageIdentifier != null ? m_MessageIdentifier.hashCode() : 0);
      result = 31 * result + (m_Score != +0.0f ? Float.floatToIntBits(m_Score) : 0);
      return result;
    }//hashCode

  }//inner class SimpleMessageHit

}//class MessageIndex
