/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.postoffice.model.*;

import java.io.IOException;
import java.util.*;

/**
 * This class implements a {@link POBox}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class POBoxImpl
    implements POBox {

  protected PostOfficeStore m_Store;
  protected MessageManager m_MessageManager;
  protected AgentIdentifier m_Owner;
  protected boolean m_Forwarding;
  protected boolean m_Private;
  protected boolean m_Dirty = false;
  protected boolean m_Notify = false;
  protected BlacklistImpl m_Blacklist;
  protected Locale m_Language;
  protected boolean m_IndexRebuild = false;
  protected MessageIndex m_MessageIndex;

  public POBoxImpl(PostOfficeStore store, AgentIdentifier owner, Locale lang, boolean forwarding, boolean priv, boolean notify, boolean index) {
    m_Store = store;
    m_MessageManager = m_Store.getMessageManager();
    m_Owner = owner;
    m_Forwarding = forwarding;
    m_Private = priv;
    m_Dirty = false;
    m_Language = lang;
    m_Blacklist = new BlacklistImpl(m_Store, m_Owner);
    m_Notify = notify;
    m_IndexRebuild = index;
  }//constructor

  public AgentIdentifier getOwner() {
    return m_Owner;
  }//getOwner

  public boolean isForwarding() {
    return m_Forwarding;
  }//isForwarding

  public void setForwarding(boolean b) {
    if (m_Forwarding != b) {
      m_Dirty = true;
      m_Forwarding = b;
    }
  }//setForwarding

  public Locale getLanguage() {
    return m_Language;
  }//getLanguage

  public void setLanguage(Locale l) {
    if (!l.equals(m_Language)) {
      m_Language = l;
      m_Dirty = true;
    }
  }//setLanguage

  public List<String> listMessages()
      throws PostOfficeServiceException {

    return m_MessageManager.listMessages(this);
  }//listMessages

  public List<String> listMessages(int from, int limit)
      throws PostOfficeServiceException {

    //we could depend on the database, but only if all databases support
    //query result ranges
    return listMessages().subList(from, from + limit);
  }//listMessages

  public int getTotalMessageCount()
      throws PostOfficeServiceException {

    return m_MessageManager.getMessageCount(this);
  }//getTotalMessageCount

  public List<String> listUnreadMessages()
      throws PostOfficeServiceException {

    return m_MessageManager.listUnread(this);
  }//listUnreadMessages

  public boolean hasUnreadMessages() throws PostOfficeServiceException {
    return m_MessageManager.hasUnread(this);
  }//hasUnreadMessages

  public MessageDescriptor getMessageDescriptor(String id)
      throws NoSuchMessageException, PostOfficeServiceException {
    return m_MessageManager.getMessage(this, id);
  }//getMessageDescriptor

  public MessageBody getMessageBody(String id)
      throws NoSuchMessageException, PostOfficeServiceException {

    return m_MessageManager.getMessageBody(this, id);
  }//getMessageBody

  public void removeMessage(String id)
      throws NoSuchMessageException, PostOfficeServiceException {

    m_MessageManager.removeMessage(this, id);
    flagIndexRebuild();
  }//removeMessage

  public List<MessageHit> searchMessages(String query, int numhits)
      throws PostOfficeServiceException {
    if (m_MessageIndex == null) {
      m_MessageIndex = new MessageIndex(this);
      try {
        m_MessageIndex.init();
      } catch (IOException ex) {
        throw new PostOfficeServiceException("searchEntries()", ex);
      }
    }
    synchronized (m_MessageIndex) {
      return m_MessageIndex.search(query, numhits);
    }
  }//searchEntries

  public void rebuildIndex()
      throws PostOfficeServiceException {
    if (!isIndexRebuild()) {
      return;
    }
    if (m_MessageIndex == null) {
      m_MessageIndex = new MessageIndex(this);
      try {
        m_MessageIndex.init();
      } catch (IOException ex) {
        Activator.log().error("rebuildIndex()", ex);
        throw new PostOfficeServiceException("rebuildIndex()", ex);
      }
    }
    try {
      m_MessageIndex.rebuildIndex(m_Store);
      clearIndexRebuild();
    } catch (IOException ex) {
      Activator.log().error("rebuildIndex()", ex);
    }
  }//rebuildIndex

  public void markRead(String id)
      throws NoSuchMessageException, PostOfficeServiceException {
    m_MessageManager.markRead(this, id);
    flagIndexRebuild();
  }//markRead

  public void setPrivate(boolean b) {
    if (m_Private != b) {
      m_Dirty = true;
      m_Private = b;
    }
  }//setPrivate

  public boolean isPrivate() {
    return m_Private;
  }//isPrivate

  public boolean isNotify() {
    return m_Notify;
  }//isNotify

  public void setNotify(boolean b) {
    if (m_Notify != b) {
      m_Dirty = true;
      m_Notify = b;

      //Ensure subscription
      if (b) {
        Activator.getServices().getNotificationManager()
            .ensureSubscription(m_Owner);
      }
    }
  }//setNotify

  public List<AgentIdentifier> getBlacklisted() {
    List<AgentIdentifier> bl = new ArrayList<AgentIdentifier>(m_Blacklist.size());
    for (Iterator<AgentIdentifier> iter = m_Blacklist.iterator(); iter.hasNext();) {
      bl.add(iter.next());
    }
    return Collections.unmodifiableList(bl);
  }//getBlacklisted

  public boolean addBlacklisted(AgentIdentifier aid) {
    return m_Blacklist.add(aid);
  }//addBlacklisted

  public boolean removeBlacklisted(AgentIdentifier aid) {
    return m_Blacklist.remove(aid);
  }//removeBlacklisted

  public boolean isBlacklisted(AgentIdentifier aid) {
    return m_Blacklist.contains(aid);
  }//isBlacklisted

  public boolean isDirty() {
    return m_Dirty;
  }//isDirty

  public void setDirty(boolean dirty) {
    m_Dirty = dirty;
  }//setDirty

  public boolean isIndexRebuild() {
    return m_IndexRebuild;
  }//isIndexRebuild

  public void flagIndexRebuild() {
    if (!m_IndexRebuild) {
      m_IndexRebuild = true;
      m_Dirty = true;
    }
  }//flagIndexRebuild

  public void clearIndexRebuild() {
    m_IndexRebuild = false;
    m_Dirty = true;
  }//clearIndexRebuild

  public static final int INDEX_UPTODATE = 0;
  public static final int INDEX_NEEDSUPDATE = 1;
  public static final int INDEX_NEEDSREBUILD = -1;

}//class POBoxImpl
