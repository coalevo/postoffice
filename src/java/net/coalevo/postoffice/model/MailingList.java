/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Identifiable;

import java.util.Set;

/**
 * This interface defines a mailing list for bulk messages.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MailingList
    extends Identifiable {

  /**
   * Adds an agent to this list.
   *
   * @param identifier the {@link AgentIdentifier} identifying the agent.
   * @return true if added, false otherwise.
   */
  public boolean add(AgentIdentifier identifier);

  /**
   * Adds agents to this list.
   *
   * @param identifiers a <tt>Set</tt> of {@link AgentIdentifier} instances identifying the agents.
   */
  public void add(Set<AgentIdentifier> identifiers);

 /**
  * Removes an agent from this list.
  *
  * @param identifier the {@link AgentIdentifier} identifying the agent.
  * @return true if removed, false otherwise.
  */
  public boolean remove(AgentIdentifier identifier);

  /**
   * Removes agents from this list.
   *
   * @param identifiers a <tt>Set</tt> of {@link AgentIdentifier} instances identifying the agents.
   */
  public void remove(Set<AgentIdentifier> identifiers);

  /**
   * Returns the recipients of this list.
   * @return a <tt>Set</tt> of {@link AgentIdentifier} instances identifying the recipients.
   */
  public Set<AgentIdentifier> getRecipients();

  /**
   * Tests if this list is generated automatically by the system.
   * <p>
   * This flag tags lists that may be maintained by the system to allow communication
   * between for example members of certain roles.
   * </p>
   * @return true if system, false otherwise.
   */
  public boolean isSystem();

  /**
   * Tests if this list allows it's members to mail to the list.
   * <p>
   * If members are allowed to mail to the list they are a member of, then
   * members will be able to do so bypassing the explicit service policy.
   * </p>
   * @return true if system, false otherwise.
   */
  public boolean isAllowsMembers();

}//interface MailingList
