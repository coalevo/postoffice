/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

/**
 * This interface defines and editable message.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableMessage
    extends MessageDescriptor {

  /**
   * Sets the subject of the message.
   *
   * @param str the subject of the entry.
   */
  public void setSubject(String str);
  
  /**
   * Sets the format of the body.
   *
   * @param str a string identifying the body format.
   */
  public void setBodyFormat(String str);

  /**
   * Returns the content of this <tt>EditableMessage</tt>.
   * @return the {@link EditableMessageBody}.
   */
  public EditableMessageBody getMessageBody();

}//interface EditableMessage
