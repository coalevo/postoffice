/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

import net.coalevo.foundation.model.AgentIdentifier;

import java.util.List;
import java.util.Locale;

/**
 * This interface defines a post box.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface POBox {

  /**
   * Returns the owner of this postbox.
   *
   * @return the owner as {@link AgentIdentifier}.
   */
  public AgentIdentifier getOwner();

  /**
   * Returns the language of this postbox.
   *
   * @return the language as <tt>Locale</tt>.
   */
  public Locale getLanguage();

  /**
   * Tests if this postbox forwards messages to the owner's email.
   *
   * @return true if forwarding, false otherwise.
   */
  public boolean isForwarding();

  /**
   * Sets the flag that determines if this postbox forwards messages to the owner's email.
   *
   * @param b true if forwarding, false otherwise.
   */
  public void setForwarding(boolean b);

  /**
   * Lists the messages in this postbox.
   *
   * @return a <tt>List</tt> of message identifiers.
   * @throws PostOfficeServiceException if the service fails to obtain the messages
   *                                    from the store.
   */
  public List<String> listMessages() throws PostOfficeServiceException;

  /**
   * Lists the messages in this postbox.
   *
   * @param from  the first message to list from.
   * @param limit the amount of messages to be listed.
   * @return a <tt>List</tt> of message identifiers.
   * @throws PostOfficeServiceException if the service fails to obtain the messages
   *                                    from the store.
   */
  public List<String> listMessages(int from, int limit)
      throws PostOfficeServiceException;

  /**
   * Returns the total message count in this postbox.
   *
   * @return the total number of messages in this postbox.
   * @throws PostOfficeServiceException if the service fails to obtain the count.
   */
  public int getTotalMessageCount() throws PostOfficeServiceException;

  /**
   * Lists the unread messages in this postbox.
   *
   * @return a <tt>List</tt> of message identifiers.
   * @throws PostOfficeServiceException if the service fails to obtain the messages
   *                                    from the store.
   */
  public List<String> listUnreadMessages() throws PostOfficeServiceException;

  /**
   * Tests if this postbox has unread messages.
   *
   * @return true if there are unread messages in this postbox, false otherwise.
   * @throws PostOfficeServiceException if the service fails to obtain the messages
   *                                    from the store.
   */
  public boolean hasUnreadMessages() throws PostOfficeServiceException;

  /**
   * Returns a {@link MessageDescriptor} for a given message identifier.
   *
   * @param id the identifier of the message.
   * @return the corresponding {@link MessageDescriptor}.
   * @throws NoSuchMessageException     if a message with such a descriptor does not exist.
   * @throws PostOfficeServiceException if the service fails to obtain the messages
   *                                    from the store.
   */
  public MessageDescriptor getMessageDescriptor(String id)
      throws NoSuchMessageException, PostOfficeServiceException;

  /**
   * Returns a {@link MessageBody} for a given message identifier.
   *
   * @param id the identifier of the message.
   * @return the corresponding {@link MessageBody}.
   * @throws NoSuchMessageException     if a message with such a descriptor does not exist.
   * @throws PostOfficeServiceException if the service fails to obtain the messages
   *                                    from the store.
   */
  public MessageBody getMessageBody(String id)
      throws NoSuchMessageException, PostOfficeServiceException;

  /**
   * Removes the message with the given message identifier.
   *
   * @param id the identifier of the message.
   * @throws NoSuchMessageException     if a message with such a descriptor does not exist.
   * @throws PostOfficeServiceException if the service fails to obtain the messages
   *                                    from the store.
   */
  public void removeMessage(String id)
      throws NoSuchMessageException, PostOfficeServiceException;

  /**
   * Searches for messages with a given lucene query.
   * <p>
   * Also the number of hits should be ranged to make sure you don't
   * get too much hits.
   * </p>
   *
   * @param query   the query.
   * @param numhits the number of max hits to be returned.
   * @return A <tt>List</tt> of {@link MessageHit} instances identifying the hit.
   * @throws PostOfficeServiceException if an error occurs while executing this action.
   */
  public List<MessageHit> searchMessages(String query, int numhits)
      throws PostOfficeServiceException;

  /**
   * Marks a message as read.
   *
   * @param id the identifier of the message.
   * @throws NoSuchMessageException     if a message with such a descriptor does not exist.
   * @throws PostOfficeServiceException if the service fails to obtain the messages
   *                                    from the store.
   */
  public void markRead(String id) throws NoSuchMessageException, PostOfficeServiceException;

  //**** Privacy options ****//

  /**
   * Sets the flag that indicates that this postbox is in private mode.
   * <p>
   * The private mode indicates that only presence subscribers (e.g. contacts)
   * can deposit messages.
   * </p>
   *
   * @param b true if private mode, false otherwise.
   */
  public void setPrivate(boolean b);

  /**
   * Tests if this postbox is in private mode.
   *
   * @return true if in private mode, false otherwise.
   */
  public boolean isPrivate();

  /**
   * Tests if this postbox is in notification mode.
   *
   * @return true if notification mode, false otherwise.
   */
  public boolean isNotify();

  /**
   * Sets the flag that indicates whether this postbox is in notification
   * mode or not.
   * <p>
   * The notification mode indicates that if the owner is present, it
   * will be notified of the deposited message.
   * </p>
   *
   * @param b true if notification mode, false otherwise.
   */
  public void setNotify(boolean b);

  /**
   * Returns the blacklisted senders.
   * <p>
   * Blacklisted senders are not allowed to deposit messages in this postbox.
   * </p>
   *
   * @return a <tt>List</tt> of {@link AgentIdentifier} instances.
   */
  public List<AgentIdentifier> getBlacklisted();

  /**
   * Adds a sender to the blacklist.
   * <p>
   * Blacklisted senders are not allowed to deposit messages in this postbox.
   * </p>
   *
   * @param aid the agent to be blacklisted as {@link AgentIdentifier} instance.
   * @return true if added, false otherwise.
   */
  public boolean addBlacklisted(AgentIdentifier aid);

  /**
   * Removes a sender from the blacklist.
   *
   * @param aid the agent to be removed from the blacklist as {@link AgentIdentifier} instance.
   * @return true if removed, false otherwise.
   */
  public boolean removeBlacklisted(AgentIdentifier aid);

  /**
   * Tests if an agent is blacklisted.
   *
   * @param aid the agent to be checked against the blacklist as {@link AgentIdentifier} instance.
   * @return true if blacklisted, false otherwise.
   */
  public boolean isBlacklisted(AgentIdentifier aid);

  /**
   * Tests if this postbox is scheduled for an index rebuild at maintenance.
   * @return true if index will be rebuilt, false otherwise.
   */
  public boolean isIndexRebuild();

}//interface POBox
