/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

import java.io.IOException;
import java.io.OutputStream;

/**
 * This interface defines a message body.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessageBody {

  /**
   * Returns the content of the body.
   *
   * @return the body as string.
   */
  public String getContent();

  /**
   * Writes the content to the given output stream.
   *
   * @param out the <tt>OutputStream</tt>.
   * @throws IOException if an I/O error occurs.
   */
  public void writeTo(OutputStream out) throws IOException;

  /**
   * Tests if the body is empty.
   *
   * @return true if empty, false otherwise.
   */
  public boolean isEmpty();

}//interface MessageBody
