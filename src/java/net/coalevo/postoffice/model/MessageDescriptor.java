/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Identifiable;

/**
 * This interface defines a descriptor for a message in a post box.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessageDescriptor
    extends Identifiable {

  /**
   * Returns the identifier of the ancestor message.
   *
   * @return the ancestors identfier.
   */
  public String getAncestor();

  /**
   * Returns the message type.
   *
   * @return the type of the message as {@link MessageType}.
   */
  public MessageType getMessageType();

  /**
   * Returns the timestamp of this message.
   * <p>
   * The timestamp indicates the time and date when this message was
   * deposited (UTC reference; no Timezone).
   * </p>
   *
   * @return the timestamp of this message as milliseconds since UTC.
   */
  public long getTimestamp();

  /**
   * Tests if this message was read.
   *
   * @return true if read, false otherwise.
   */
  public boolean isRead();

  /**
   * Returns the sender of the message.
   *
   * @return the sender of the message as {@link AgentIdentifier}.
   */
  public AgentIdentifier getFrom();

  /**
   * Returns the recipient of the message.
   *
   * @return the recipient of the message as {@link AgentIdentifier}.
   */
  public AgentIdentifier getTo();

  /**
   * Returns the list (identifier) this message was sent through.
   *
   * @return the identifier of the list this message was sent through.
   */
  public String getVia();

  /**
   * Returns the subject of the message.
   *
   * @return the subject as <tt>String</tt>.
   */
  public String getSubject();

  /**
   * Returns the format identifier of the body format.
   *
   * @return the body format identifier as <tt>String</tt>.
   */
  public String getBodyFormat();

  /**
   * Tests if this message was received.
   * <p>
   * This is essentially a convenience method that compares the
   * recipient to the postbox owner.
   * </p>
   * @return true if received, false otherwise.
   */
  public boolean isReceived();

  /**
   * Tests if this message was sent through a list.
   * <p>
   * This is essentially a convenience method that will
   * check the via for a valid identifier.
   * </p>
   * @return true if list, false otherwise.
   */
  public boolean isListMessage();

}//interface MessageDescriptor
