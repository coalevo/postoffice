/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

/**
 * This class defines tokens the indices and XML representation
 * of post office data.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PostOfficeTokens {

  public static final String ELEMENT_MESSAGE = "message";
  public static final String ATTR_POBOX = "pobox";
  public static final String ATTR_ID = "id";
  public static final String ATTR_ANCESTOR = "ancestor";
  public static final String ATTR_READ = "read";

  public static final String ELEMENT_HEADERS = "headers";
  public static final String ELEMENT_DEPOSITED = "deposited";
  public static final String ELEMENT_TYPE = "type";
  public static final String ELEMENT_FROM = "from";
  public static final String ELEMENT_TO = "to";
  public static final String ELEMENT_VIA = "via";
  public static final String ELEMENT_SUBJECT = "subject";

  public static final String ELEMENT_BODY = "body";
  public static final String ATTR_FORMAT = "format";

}//class PostOfficeTokens
