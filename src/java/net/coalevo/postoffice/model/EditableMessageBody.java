/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

import java.io.IOException;
import java.io.InputStream;

/**
 * This interface defines an ditable message body.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableMessageBody
    extends MessageBody {

  /**
   * Sets the content.
   *
   * @param content a String.
   */
  public void setContent(String content);

  /**
   * Sets the content from the given <tt>InputStream</tt>.
   *
   * @param in the <tt>InputStream</tt> to obtain the content from.
   * @throws IOException if I/O fails.
   */
  public void setContent(InputStream in) throws IOException;

  /**
   * Returns a new non-editable content instance.
   *
   * @return a {@link MessageBody} instance.
   */
  public MessageBody toMessageBody();

}//interface EditableMessageBody
