/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

/**
 * Exception thrown when an error occurs in a post office service action.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PostOfficeServiceException extends Exception {

  public PostOfficeServiceException() {
  }//PostOfficeServiceException

  public PostOfficeServiceException(String message) {
    super(message);
  }//PostOfficeServiceException

  public PostOfficeServiceException(String message, Throwable cause) {
    super(message, cause);
  }//PostOfficeServiceException

  public PostOfficeServiceException(Throwable cause) {
    super(cause);
  }//PostOfficeServiceException

}//class PostOfficeServiceException
