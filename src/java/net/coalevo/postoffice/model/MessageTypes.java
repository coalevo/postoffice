/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.model;

import java.util.*;

/**
 * Provides all defined {@link MessageType}s.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class MessageTypes {

  /**
   * Defines a {@link MessageType} for a message
   * send between users.
   */
  public static final MessageType MESSAGE = new TypeImpl("message");

  /**
   * Defines a {@link MessageType} for a message
   * that is notifying the user of something. This message will not
   * require a reply, and will likely be produced by some service of
   * the system for a particular user.
   */
  public static final MessageType NOTIFICATION = new TypeImpl("notification");

  /**
   * Defines a {@link MessageType} for a message
   * that is distributed on a list. These messages may not require a
   * reply.
   */
  public static final MessageType BULK = new TypeImpl("bulk");

  /**
    * Array holding the defined {@link MessageType} instances.
    */
   protected static final MessageType[] TYPES = new MessageType[]{MESSAGE, NOTIFICATION, BULK};

   /**
    * Set holding the defined {@link MessageType} instances.
    */
   protected static final Set<MessageType> MESSAGE_TYPES =
       Collections.unmodifiableSet(new HashSet<MessageType>(Arrays.asList(TYPES)));

   /**
    * Returns a {@link MessageType} instance for the given identifier.
    *
    * @param identifier a string identifying a {@link MessageType}.
    * @return the {@link MessageType} with the given identifier.
    * @throws NoSuchElementException if a {@link MessageType} with
    *                                the given identifier does not exist.
    */
   public final static MessageType get(String identifier)
       throws NoSuchElementException {
     for (MessageType messageType : MESSAGE_TYPES) {
       if (messageType.getIdentifier().equals(identifier)) {
         return messageType;
       }
     }
     throw new NoSuchElementException(identifier);
   }//get

  
  static final class TypeImpl
      implements MessageType {

    private final String m_Identifier;

    protected TypeImpl(String id) {
      m_Identifier = id;
    }//constructor

    public String getIdentifier() {
      return m_Identifier;
    }//getIdentifier

    public String toString() {
      return m_Identifier;
    }//toString

    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof TypeImpl)) return false;

      final TypeImpl type = (TypeImpl) o;

      if (!m_Identifier.equals(type.m_Identifier)) return false;

      return true;
    }//equals

    public int hashCode() {
      return (m_Identifier != null ? m_Identifier.hashCode() : 0);
    }//hashCode

  }//inner class TypeImpl

}//class MessageTypes
