/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.service;

/**
 * Defines a tag interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see net.coalevo.foundation.util.BundleConfiguration
 */
public interface PostOfficeConfiguration {

  public static final String DATA_SOURCE_KEY = "datasource";
  public static final String CONNECTION_POOLSIZE_KEY = "connections.poolsize";
  public static final String POBOX_CACHE_SIZE_KEY = "cache.pobox.size";
  public static final String MESSAGE_CACHE_SIZE_KEY = "cache.message.size";
  public static final String LIST_CACHE_SIZE_KEY = "cache.mailinglist.size";
  public static final String AID_CACHE_SIZE_KEY = "cache.agentidentifiers.size";
  public static final String MEMBERSHIPS_CACHE_SIZE_KEY = "cache.memberships.size";
  public static final String NOUNREAD_CACHE_SIZE_KEY = "cache.nounread.size";

}//interface PostOfficeConfiguration
