/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.postoffice.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Service;
import net.coalevo.postoffice.model.*;

import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * This interface defines the post office service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PostOfficeService
    extends Service {

  /**
   * Creates a new post box.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    the {@link AgentIdentifier} identifying the agent the box will belong to.
   * @param l      the <tt>Locale</tt> that defines the main language for this post box.
   * @param forw   flags if the POBox will forward new messages to the users email address.
   * @param priv   flags if the POBox is private (only presence subscribers can deposit messages).
   * @param notify flags if the POBox will notify the owner through interactive messaging if present.
   * @return the created {@link POBox} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws PostOfficeServiceException if an error occurs creating the post box.
   */
  public POBox createPostBox(Agent caller, AgentIdentifier aid, Locale l, boolean forw, boolean priv, boolean notify)
      throws SecurityException, PostOfficeServiceException;

  /**
   * Returns the caller's post box.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    the {@link AgentIdentifier} identifying the agent the box belongs to.
   * @return a {@link POBox} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws NoSuchPOBoxException       if no postbox exists for the calling agent.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public POBox getPostBox(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchPOBoxException, PostOfficeServiceException;

  /**
   * Destroys the post box of an existing agent.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    the {@link AgentIdentifier} identifying the agent the box will belong to.
   * @return true if created, false otherwise.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws NoSuchPOBoxException       if the specified agent does not own a post box.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public boolean removePostBox(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchPOBoxException, PostOfficeServiceException;

  /**
   * Creates a new mailing list.
   *
   * @param caller     the calling {@link Agent}.
   * @param identifier the identifier of the list.
   * @param allowmem   the flag that determines if members should be allowed to mail to this list.
   * @return the created {@link MailingList}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public MailingList createList(Agent caller, String identifier, boolean allowmem) throws SecurityException, PostOfficeServiceException;

  /**
   * Destroys an existing mailing list.
   *
   * @param caller     the calling {@link Agent}.
   * @param identifier the identifier of the list.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws NoSuchListException        if a list with the given identifier does not exist.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public void removeList(Agent caller, String identifier)
      throws SecurityException, NoSuchListException, PostOfficeServiceException;

  /**
   * Returns a mailing list.
   *
   * @param caller     the calling {@link Agent}.
   * @param identifier the identifier of the list.
   * @return the corresponding {@link MailingList} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws NoSuchListException        if a list with the given identifier does not exist.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public MailingList getList(Agent caller, String identifier)
      throws SecurityException, NoSuchListException, PostOfficeServiceException;

  /**
   * Returns all identifiers for all existing mailing list.
   *
   * @param caller the calling {@link Agent}.
   * @param system flags if looking for system lists or user defined ones.
   * @return a <tt>Set</tt> of <tt>String</tt> instances.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public Set<String> getLists(Agent caller, boolean system)
      throws SecurityException, PostOfficeServiceException;

  /**
   * Returns all identifiers for all existing mailing list.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    the agent whose memberships are sought.
   * @return a <tt>List</tt> of <tt>String</tt> instances.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public List<String> getListMemberships(Agent caller, AgentIdentifier aid)
      throws SecurityException, PostOfficeServiceException;

  /**
   * Tests if the given caller may send to the given mailing list.
   *
   * @param caller   the calling {@link Agent}.
   * @param listname the name of the mailing list.
   * @return true if allowed, false otherwise.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public boolean isSendToListAllowed(Agent caller, String listname)
      throws SecurityException, PostOfficeServiceException;

  /**
   * Returns an editable message to be deposited using
   * {@link #depositMessage(Agent,EditableMessage)}
   * {@link #cancelMessage(Agent,EditableMessage)}.
   *
   * @param caller the calling {@link Agent}.
   * @param to     the recipient of the message.
   * @return an {@link EditableMessage}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public EditableMessage beginMessage(Agent caller, AgentIdentifier to)
      throws SecurityException, PostOfficeServiceException;

  /**
   * Begins a list message to be deposited using
   * {@link #depositMessage(Agent,EditableMessage)}, or cancelled using
   * {@link #cancelMessage(Agent,EditableMessage)}.
   *
   * @param caller the calling {@link Agent}.
   * @param listid the identifier of the list.
   * @return an {@link EditableMessage}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public EditableMessage beginListMessage(Agent caller, String listid)
      throws SecurityException, PostOfficeServiceException;

  /**
   * Begins a list message to be deposited using
   * {@link #depositMessage(Agent,EditableMessage)}, or cancelled using
   * {@link #cancelMessage(Agent,EditableMessage)}.
   * <p>
   * This method allows a service to deposit a message in behalf of a user
   * that may not be authorized to do so otherwise.
   * </p>
   *
   * @param caller the calling {@link Agent} on behalf of the author (from).
   * @param listid the identifier of the list.
   * @param from the {@link AgentIdentifier} specifying the actual author.
   * @return an {@link EditableMessage}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public EditableMessage beginListMessage(Agent caller, String listid, AgentIdentifier from)
      throws SecurityException, PostOfficeServiceException;

  /**
   * Cancels a message that was started using
   * {@link #beginMessage(Agent,AgentIdentifier)}
   * or {@link #beginListMessage(Agent,String)}.
   *
   * @param caller the calling {@link Agent}.
   * @param emsg   an {@link EditableMessage}.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   * @throws NoSuchMessageException if the {@link EditableMessage} instance was not provided by this service
   *                                through {@link #beginMessage(Agent,AgentIdentifier)} or
   *                                {@link #beginListMessage(Agent,String)}.
   */
  public void cancelMessage(Agent caller, EditableMessage emsg)
      throws SecurityException, NoSuchMessageException;

  /**
   * Deposits a message that was started using
   * {@link #beginMessage(Agent,AgentIdentifier)}
   * or {@link #beginListMessage(Agent,String)}.
   *
   * @param caller the calling {@link Agent}.
   * @param emsg   {@link EditableMessage} instance obtained through
   *               {@link #beginMessage(Agent,AgentIdentifier)} or {@link #beginListMessage(Agent,String)}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws NoSuchMessageException     if the {@link EditableMessage} instance was not provided by this service
   *                                    through {@link #beginMessage(Agent,AgentIdentifier)} or
   *                                    {@link #beginListMessage(Agent,String)}.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public void depositMessage(Agent caller, EditableMessage emsg)
      throws SecurityException, NoSuchMessageException, PostOfficeServiceException;

  /**
   * Rebuilds the index of the postbox corresponding to the given agent.
   *
   * @param caller the calling {@link Agent}.
   *               <p>
   *               Note that this method will run an asynchronous task to rebuild the index
   *               and return immediately.
   *               </p>
   * @param aid    specifies the agent whose postbox index should be rebuild.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws NoSuchPOBoxException       if the specified agent does not own a post box.
   * @throws PostOfficeServiceException if an error occurs in the backend.
   */
  public void rebuildPOBoxIndex(Agent caller, final AgentIdentifier aid)
      throws SecurityException, NoSuchPOBoxException, PostOfficeServiceException;

}//interface PostOfficeService
